# About this project #

In this project I implemented visualizations for a number of search algorithms that show how the algorithms work. I created this for first year college students following an introductory course on artificial intelligence at the VU. It is used as extra study material to help teach them how these search algorithms work.

For each of the search algorithms I made three ways of visualizing them: a tree, a maze and the search space of an eightpuzzle being displayed as a tree. The tree gives the easiest insight in how the algorithms make their decisions. The maze is used to show how the algorithms can be applied to a real life scenario. And the eightpuzzle gives a better intuition on how the search-space can often be seen as a tree.

Included search algorithms: breadth first search, depth first search, iterative deepening, hill climbing, best first search, beam search, algoritme A, algoritme A*, MiniMax.

Used technologies: Javascript, CSS, HTML