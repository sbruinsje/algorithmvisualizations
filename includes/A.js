function A( rootNode, goalNode, getChildren ){
	this.rootNode = rootNode;
	this.goalNode = goalNode;
	this.getChildren = getChildren;
	this.closedList = new Vector();
	this.openList = new Vector();
	this.goalReached = false;
	this.done = false;
	this.stepNumber = 0;
	this.start();
}


// Start by adding the root node.
A.prototype.start = function() {
	this.openList.addFirst( this.rootNode );
}



A.prototype.step = function(){
	// check if the algorithm is done, if its we can't step() further.
	if( this.done == true ){ 
		return this.closedList;
	}	
	
	this.stepNumber+=1;

	// Get the node with the best heuristic and remove it from the openList
	var bestHeuristic = Number.POSITIVE_INFINITY;
	var currentNode = null;
	for( var i=0; i<this.openList.getSize(); i++ ){
		var node = this.openList.getElementAt( i );
		realNodeHeuristic = node.heuristic + node.depth;
		if( realNodeHeuristic < bestHeuristic ){
			currentNode = node;
			bestHeuristic = realNodeHeuristic;
		}
	}
	this.openList.removeElement( currentNode );

	// Check if the current node is the node we are looking for.
	if(  currentNode.equalData( this.goalNode ) ){
		this.closedList.addElement( currentNode );
		this.goalReached = true;
		this.done = true;
		return this.closedList;
	}

	// the current node is not the node we are looking for.. so get its children. 
	// This is done by using the function pointer that we passed as argument. 
	var children = this.getChildren( currentNode );
	for( var i=0; i<children.getSize(); i++ ){
		// Add all of the current node its children to the front of the open list of nodes.
		this.openList.addElement( children.getElementAt(i) );
	}
	
	
	//check if there are nodes in openList. if not then the algorithm is done, no more nodes to search..
	if( this.openList.getSize() == 0 ){ 
		this.done = true;
	}

	this.closedList.addElement( currentNode );
	return this.closedList;
}


// Returns whether the algorithm is finnished.
A.prototype.isDone = function(){
	return this.done;
}


// Resets the values of the BreadthFirstSearch object, so we can start all over again.
A.prototype.reset = function(){
	this.openList = new Vector();
	this.closedList = new Vector();
	this.done = false;
	this.stepNumber = 0;
	this.goalReached = false;
	this.start();
}


A.prototype.undoStep = function(){
	var stepNumber = this.stepNumber;
	if( stepNumber > 0 ){
		this.reset();
		for( var i=0; i<stepNumber-1; i++ ){
			this.step();
		}
	}
}