/*
	This object can perform a Beam Search algorithm. It can be used for many  applications. The applications need to:
	- specify the start node
	- specify the goal node
	- Specify where to get the children for a given node.
	- Communicate with this object through SearchNode objects. For example it must return the children for a given node wrapped in a search node object.
	- The data field of the SearchNodes must contain data of a primitive type or contain a .equals method to compare it.
*/
function BeamSearch( rootNode, goalNode, getChildren, beamSize ){
	this.rootNode = rootNode;
	this.goalNode = goalNode;
	this.getChildren = getChildren;
	this.closedList = new Vector();
	this.openList = new Vector();
	this.goalReached = false;
	this.done = false;
	this.beamSize = beamSize;
	this.discardedList = new Vector(); // will  hold all openNodes that are discarded because they fall outside of the beamSize range.
	this.stepNumber = 0;
	this.start();
}

/*
	Start by adding the root node.
*/
BeamSearch.prototype.start = function() {
	this.openList.addFirst( this.rootNode );
}

/*
	Perform a step in the algorithm
*/
BeamSearch.prototype.step = function(){
	// check if the algorithm is done, if its we can't step() further.
	if( this.done == true ){ 
		return this.closedList;
	}
	this.stepNumber+=1;

	// Get the node with the best heuristic and remove it from the openList
	currentNode = this.getNodeWithBestHeuristic();
	this.openList.removeElement( currentNode );

	// Check if the current node is the node we are looking for.
	if(  currentNode.equalData( this.goalNode ) ){
		this.closedList.addElement( currentNode );
		this.goalReached = true;
		this.done = true;
		return this.closedList;
	}

	// the current node is not the node we are looking for.. so get its children. 
	// This is done by using the function pointer that we passed as argument. 
	var children = this.getChildren( currentNode );
	for( var i=0; i<children.getSize(); i++ ){
		// Add all of the current node its children to the front of the open list of nodes.
		this.openList.addElement( children.getElementAt(i) );
	}

	// Keep nodes with the best heuristic, the number of nodes to keep is defined by the 'this.beamSize'.
	this.onlyKeepBestNodes();
	
	
	//check if there are nodes in openList. if not then the algorithm is done, no more nodes to search..
	if( this.openList.getSize() == 0 ){ 
		this.done = true;
	}

	this.closedList.addElement( currentNode );
	return this.closedList;
}

/*
	Only keeps the nodes with the best heuristics. The number of nodes to keep is defined by the 'this.beamSize' property.
*/
BeamSearch.prototype.onlyKeepBestNodes = function(){
	// If the number of nodes in openList is bigger than the allowed beamSize, then we need to discard some nodes.
	if( this.openList.getSize() > this.beamSize ){
		var newOpenList = new Vector();
		while( newOpenList.getSize() < this.beamSize ){
			var child = this.getNodeWithBestHeuristic();
			newOpenList.addElement( child );
			this.openList.removeElement( child );
		}
		
		// All elements that are still in the openList will be discarded. We add these to the discardedList.
		for( var i=0; i<this.openList.getSize(); i++ ){
			this.discardedList.addElement( this.openList.getElementAt( i ) );
		}

		// update openList by replacing it with the newOpenList which only contains the top k nodes (k='this.beamSize').
		this.openList = newOpenList;
	}
}

/*
	get the node with the best heuristic.
*/
BeamSearch.prototype.getNodeWithBestHeuristic = function(){
	var bestHeuristic = Number.POSITIVE_INFINITY;
	var nodeWithBestHeuristic = null;
	for( var i=0; i<this.openList.getSize(); i++ ){
		node = this.openList.getElementAt( i );
		if( node.heuristic < bestHeuristic ){
			nodeWithBestHeuristic = node;
			bestHeuristic = node.heuristic;
		}
	}
	
	if( nodeWithBestHeuristic === null ){
		throw new Error('Failed to get the node with the best heuristic. Could be because there are no nodes in the openList, or because the minimum heuristic is equal to Number.POSITIVE_INFINITY.');
	}

	return nodeWithBestHeuristic;
}

/*
	Returns whether the algorithm is finished.
*/
BeamSearch.prototype.isDone = function(){
	return this.done;
}

/*
	Sets the beam size used for the algorithm
*/
BeamSearch.prototype.setBeamSize = function( size ){
	this.beamSize = size;
	this.reset();
}

/*
	Resets the values of the BreadthFirstSearch object, so we can start all over again.
*/
BeamSearch.prototype.reset = function(){
	this.openList = new Vector();
	this.closedList = new Vector();
	this.discardedList = new Vector();
	this.done = false;
	this.stepNumber = 0;
	this.goalReached = false;
	this.start();
}

/*
	Undo a step in the algorithm
*/
BeamSearch.prototype.undoStep = function(){
	var stepNumber = this.stepNumber;
	if( stepNumber > 0 ){
		this.reset();
		for( var i=0; i<stepNumber-1; i++ ){
			this.step();
		}
	}
}