/*
	This object can perform a Breadth First Search algorithm. It can be used for many  applications. The applications need to:
	- specify the start node
	- specify the goal node
	- Specify where to get the children for a given node.
	- Communicate with this object through SearchNode objects. For example it must return the children for a given node wrapped in a search node object.
	- The data field of the SearchNodes must contain data of a primitive type or contain a .equals method to compare it.
*/
function BreadthFirstSearch( rootNode, goalNode, getChildren ){
	this.rootNode = rootNode;
	this.goalNode = goalNode;
	this.getChildren = getChildren;
	this.closedList = new Vector();
	this.openList = new Vector();
	this.goalReached = false;
	this.done = false;
	this.foundNode;
	this.stepNumber = 0;
	this.start();
}

/*
	Start by adding the root node.
*/
BreadthFirstSearch.prototype.start = function() {
	this.openList.addFirst( this.rootNode );
}

/*
	Take a step in the algorithm.
*/
BreadthFirstSearch.prototype.step = function(){
	if( this.done == true ){ // check if the algorithm is done, if its we can't step() further.
		return this.closedList;
	} else{
		this.stepNumber+=1;

		// Get the first node in the open list (=list of nodes to visit in order) and save it as current node.
		var currentNode = this.openList.getFirstElement();
		this.openList.removeFirstElement();
		
		// Check if the current node is the node we are looking for.
		if( currentNode.equalData( this.goalNode ) ){
			this.closedList.addElement( currentNode ); // add element at end of the closedList.
			this.goalReached = true;
			this.done = true;
			return this.closedList;
		}
		
		// the current node is not the node we are looking for.. so get its children. 
		// This is done by using the function pointer that we passed as argument. 
		var children = this.getChildren( currentNode );
		for( var i=0; i<children.getSize(); i++ ){
			// Add all of the current node its children to the end of the open list of nodes.
			this.openList.addElement( children.getElementAt(i) );
		}
		
		 // store the current node in the closed list.
		 // With the closed we can track which nodes have been visisted when.
		this.closedList.addElement( currentNode );

		
		//check if there are nodes in openList. if not then the algorithm is done, no more nodes to search..
		if( this.openList.getSize() == 0 ){ 
			this.done = true;
		}
		
		return this.closedList;
	}
}

/*
	Returns whether the algorithm is finished.
*/
BreadthFirstSearch.prototype.isDone = function(){
	return this.done;
}


/*
	Resets the values of the BreadthFirstSearch object, so we can start all over again.
*/
BreadthFirstSearch.prototype.reset = function(){
	this.openList = new Vector();
	this.closedList = new Vector();
	this.done = false;
	this.goalReached = false;
	this.stepNumber = 0;
	this.start();
}

/*
	Undo a step in the algorithm
*/
BreadthFirstSearch.prototype.undoStep = function(){
	var stepNumber = this.stepNumber;
	if( stepNumber > 0 ){
		this.reset();
		for( var i=0; i<stepNumber-1; i++ ){
			this.step();
		}
	}
}