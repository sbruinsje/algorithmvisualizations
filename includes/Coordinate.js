function Coordinate( xCoord, yCoord ){
	this.xCoord = xCoord;
	this.yCoord = yCoord;
}


Coordinate.prototype.getX = function(){
	return this.xCoord;
}


Coordinate.prototype.getY = function(){
	return this.yCoord;
}



Coordinate.prototype.setX = function( xCoord ){
	this.xCoord = xCoord;
}


Coordinate.prototype.setX = function( yCoord ){
	this.yCoord = yCoord;
}


Coordinate.prototype.applyDirection = function( direction ){
	var newX = this.xCoord + direction.getX();
	var newY = this.yCoord + direction.getY();
	return new Coordinate( newX, newY );
}


Coordinate.prototype.equals = function( coord ){
	if( coord.getX() == this.xCoord && coord.getY() == this.yCoord ){
		return true;
	} else{
		return false;
	}
}

Coordinate.prototype.clone = function(){
	return new Coordinate( this.xCoord, this.yCoord );
}