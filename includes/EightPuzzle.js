function EightPuzzle(){
	this.puzzle = new Table(3,3);	// this property holds the actual state of the puzzle (so in what position the tiles are.)
	this.emptyTile;					// this tracks the location of the empty cell.
	this.heuristic;
	this.initialize();				// This initializes the maze, to be in a certain state. (determine starting position of tiles)
}

// Define some 'constants'
EightPuzzle.prototype.DIRECTIONS = {	'N': new Coordinate(0, -1), 
										'E': new Coordinate(1, 0),
										'S': new Coordinate(0, 1),
										'W': new Coordinate(-1, 0)};



// This initializes the puzzle
EightPuzzle.prototype.initialize = function(){
	this.setSolutionState();
	this.randomize();
}



// This swaps the tile at the given coordinate with the empty tile, but only if it is a neighbour of the empty tile.
EightPuzzle.prototype.swap = function( coord ){
	if( this.isNeighbourOfEmptyTile( coord ) ){
		var coordValue = this.getTile(coord);
		this.setTile( this.emptyTile, coordValue ); // set the empty tile with the new value
		this.setTile( coord, null );				// set the tile value to the empty tile.
		this.emptyTile = coord.clone();				// update the emptyTile property
	} else{
		throw new Error("illegal move");
	}
}


// This method checks whether the current state of the eightPuzzle is the solution state.
EightPuzzle.prototype.isSolution = function(){
	var solutionState = this.getSolutionState();
	if( this.equals( solutionState ) ){
		return true;
	}
}

// This method sets the puzzle to match a certain state.
EightPuzzle.prototype.setState = function( state ){
	if( typeof state == 'undefined' || state == null || state.puzzle == null || state.emptyTile == null ){
		throw new Error('Invalid state specified' );
	}
	this.puzzle = state.puzzle;
	this.emptyTile = state.emptyTile;
	this.heuristic = state.heuristic;
}


// This sets a tile with the given coordinate to a specified value.
EightPuzzle.prototype.setTile = function( coord, value ){
	this.puzzle.store( coord.getY(), coord.getX(), value );
}


// This retrieves the value of a given tile.
EightPuzzle.prototype.getTile = function( coord ){
	return this.puzzle.retrieve( coord.getY(), coord.getX() );
}





// This sets the puzzle to the solution state.
EightPuzzle.prototype.setSolutionState = function(){
	for( var i=0; i<3; i++ ){
		for( var j=0; j<3; j++ ){
			var coord = new Coordinate( i, j );
			this.setTile( coord, i+3*j );
		}
	}
	
	this.emptyTile = new Coordinate(2,2);
	this.setTile( this.emptyTile, null );
}


// sets the puzzle in a random state by swapping the empty tile with a randomly chosen neighbour many times.
EightPuzzle.prototype.randomize = function( numberOfSwaps ){
	var numberOfSwaps = (typeof numberOfSwaps != 'undefined' && numberOfSwaps != 'null' ) ? numberOfSwaps : (500 + (Math.floor( Math.random()*100 )));
	for( var i=0; i<numberOfSwaps; i++ ){
		var neighbours = this.getNeighbourCoordinates( this.emptyTile );
		var numberOfNeighbours = neighbours.getSize();
		var randomIndex = Math.floor( Math.random()*numberOfNeighbours );	// generate a random neighbour index.
		var randomNeighbour = neighbours.getElementAt( randomIndex );
		//alert('switch empty: ' + this.emptyTile.getX() + ',' +  this.emptyTile.getY() + ') with: (' + randomNeighbour.getX() + ',' + randomNeighbour.getY() + ')' );
		this.swap( randomNeighbour );
	}
}


EightPuzzle.prototype.getCoordByValue = function( value ){
	for( var i=0; i<3; i++ ){
		for( var j=0; j<3; j++ ){
			var coord = new Coordinate( j,i);
			var currentValue = this.getTile( coord );
			if( currentValue == value ){
				return coord;
			}
		}
	}
	throw new Error("An eightpuzzle doesn't contain that value.");
}


// This returns the total manhattan distance for the given state.
EightPuzzle.prototype.determineManhattanDistance = function(){
	var solutionState = new EightPuzzle();
	solutionState.setSolutionState();
	var manhattanDistance = 0;

	for( var i=0; i<3; i++ ){
		for( var j=0; j<3; j++ ){
			var currentCoord = new Coordinate( j, i );
			var currentValue = this.getTile( currentCoord );
			var solutionCoord = solutionState.getCoordByValue( currentValue );
			var xDifference = Math.abs( currentCoord.getX() - solutionCoord.getX() );
			var yDifference = Math.abs( currentCoord.getY() - solutionCoord.getY() );
			manhattanDistance += xDifference + yDifference;			
		}
	}
	return manhattanDistance;
}



// Returns a boolean that indicates whether some otherEightPuzzle is equal to this one.
EightPuzzle.prototype.equals = function( otherEightPuzzle ){
	// walk through all the possible coordinates in the eightPuzzle
	for( var i=0; i<3; i++ ){
		for( var j=0; j<3; j++ ){
			var coord = new Coordinate( j, i );
			var thisTileValue = this.getTile( coord );
			var otherTileValue = otherEightPuzzle.getTile( coord );

			// check if these tiles have the same value.
			if( thisTileValue != otherTileValue ){
				return false;
			}

			// And to be sure, check if the emptyTile property is the same (should be if all tiles are the same, but better safe than sorry.)
			if( ! this.emptyTile.equals( otherEightPuzzle.emptyTile ) ){
				return false;
			}
		}
	}
	return true;
}





// Returns a clone of this object.
EightPuzzle.prototype.clone = function(){
	var clone = new EightPuzzle();
	// walk through all the possible coordinates in the eightPuzzle
	for( var i=0; i<3; i++ ){
		for( var j=0; j<3; j++ ){
			var coord = new Coordinate( j, i );
			
			// copy the value of the tile at coord 'coord' from this object to the clone object.
			var thisTileValue = this.getTile( coord );
			clone.setTile( coord, thisTileValue);

			// clone the emptyTile property and set it as the clones  emptyTile property
			clone.emptyTile = this.emptyTile.clone();
		}
	}
	
	return clone;
}




EightPuzzle.prototype.getNeighbourCoordinates = function( coord ){
	var neighbourCoordinates = new Vector();
	var northCoord = coord.applyDirection( this.DIRECTIONS['N'] );
	var eastCoord = coord.applyDirection( this.DIRECTIONS['E'] );
	var southCoord = coord.applyDirection( this.DIRECTIONS['S'] );
	var westCoord = coord.applyDirection( this.DIRECTIONS['W'] );
	
	if( this.isValidCoordinate(northCoord) ){
		neighbourCoordinates.addElement( northCoord );
	}
	if( this.isValidCoordinate(eastCoord) ){
		neighbourCoordinates.addElement( eastCoord );
	}
	if( this.isValidCoordinate(southCoord) ){
		neighbourCoordinates.addElement( southCoord );
	}
	if( this.isValidCoordinate(westCoord) ){
		neighbourCoordinates.addElement( westCoord );
	}
	
	return neighbourCoordinates;
}

EightPuzzle.prototype.getNumberOfNeighbourStates = function(){
	var neighbourCoordinates = this.getNeighbourCoordinates( this.emptyTile );
	return neighbourCoordinates.getSize();
}

EightPuzzle.prototype.getNeighbourStates = function(){
	var neighbourCoordinates = this.getNeighbourCoordinates( this.emptyTile );
	var neighbourStates = new Vector();
	for ( var i=0; i<neighbourCoordinates.getSize(); i++ ){
		var newState = this.clone();
		newState.swap( neighbourCoordinates.getElementAt( i ) );
		neighbourStates.addElement( newState );
	}
	return neighbourStates;
}

// This function checks in what direction 'otherCoord' lies, seen from 'coord'. (so if coord=(3,2) and otherCoord=(4,2) then this will return east.)
EightPuzzle.prototype.coordinateIsInDirection = function( coord, otherCoord){
	var coordDifferenceX =  coord.getX() - otherCoord.getX();
	var coordDifferenceY =  coord.getY() - otherCoord.getY();
	
	if( coordDifferenceX >= 1 ){
		return 'W';
	} else if( coordDifferenceX <= -1 ){
		return 'E';
	} else if( coordDifferenceY >= 1 ){
		return 'N';
	} else if( coordDifferenceY <= -1 ){
		return 'S';
	} else{
		throw new Error('Two coordinates that are neighbours of eachother must be passed as arguments.');
	}
}


// This method returns an eightPuzzle that is in the solution state.
EightPuzzle.prototype.getSolutionState = function(){
	var solutionState = new EightPuzzle();
	solutionState.setSolutionState();
	return solutionState;
}

// This function checks if the given coordinate is a neighbour of the empty tile.
EightPuzzle.prototype.isNeighbourOfEmptyTile = function( coord ){
	var neighbours = this.getNeighbourCoordinates( this.emptyTile ); // get all valid neighbours coordinates of the empty tile
	
	// check if argument 'coord' is equal to one of the neighbours, if not then it is not a neighbour of the empty tile.
	for( var i=0; i<neighbours.getSize(); i++ ){
		var neighbour = neighbours.getElementAt( i );
		if( coord.equals(neighbour) ){
			return true; // coord is a neighbour of the empty tile, so return true.
		}
	}
	return false;
}


// checks whether the specified coordinate lies within the boundarties of the eightPuzzle.
EightPuzzle.prototype.isValidCoordinate = function( coord ){
	var x = coord.getX();
	var y = coord.getY();
	if( x < 0 ){ 
		return false; 
	} else if( x >= 3 ){ 
		return false;
	} else if( y < 0 ){ 
		return false;
	} else if( y >= 3 ){ 
		return false;
	} else{ 
		return true; 
	}
}