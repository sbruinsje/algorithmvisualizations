function EightPuzzleUi( eightPuzzleId, eightPuzzle, gridSize, parentHtmlElement ){
	if( typeof eightPuzzleId == 'undefined' || typeof eightPuzzle == 'undefined' || typeof gridSize == 'undefined'){
		throw new Error('Missing argument value for EightPuzzleUi constructor.');
	}
	this.eightPuzzleId = eightPuzzleId;
	this.eightPuzzle = eightPuzzle;
	this.styleManager = new StyleManager();
	this.parentHtmlElement = parentHtmlElement;
	this.gridSize = ( typeof gridSize == 'undefined' || gridSize == null) ? 10 : gridSize;
	this.buildEightPuzzle( parentHtmlElement );
}


// This refreshes the state of the eightpuzzle on the screen.
EightPuzzleUi.prototype.refresh = function(){
	this.deleteEightPuzzle();
	this.buildEightPuzzle();
}



// This function creates the html structure for the puzzle and displays it on the page.
EightPuzzleUi.prototype.buildEightPuzzle = function( htmlElement ){
	var parentHtmlElement = ( typeof htmlElement == 'undefined' || htmlElement == null ) ? this.getParentHtmlElement() : htmlElement;
	
	// create the table which will contain the eightpuzzle
	var table = document.createElement('table');
	table.setAttribute('id',this.eightPuzzleId + '_table');
	table.setAttribute('name',this.eightPuzzleId + '_table');
	
	// add 3 rows with 3 columns to the table, these are the tiles of the eightpuzzle.
	for( var i=0; i<3; i++ ){
		var row = document.createElement('tr');
		row.setAttribute('id',this.eightPuzzleId + '_row_' + i);
		row.setAttribute('name',this.eightPuzzleId + '_row_' + i);
		table.appendChild(row);
		
		// add cells to the row.
		for( var j=0; j<3; j++ ){
			var currentCoord = new Coordinate( j, i );
			var cell = document.createElement('td');
			var cellText = (this.eightPuzzle.getTile( currentCoord ) == null) ? '' : this.eightPuzzle.getTile( currentCoord );
			var cellTextNode = document.createTextNode(cellText);
			cell.appendChild( cellTextNode );
			cell.setAttribute('id',this.eightPuzzleId + '_cell_' + j + '_' + i);
			cell.setAttribute('name',this.eightPuzzleId + '_cell_' + j + '_' + i);
			cell.style.width = this.gridSize + 'px';
			cell.style.height = this.gridSize + 'px';
			if( this.gridSize <= 20 ){
				cell.style.fontSize = 'xx-small';
			}
			row.appendChild(cell);
		}
	}
	// add the table to the parentHtmlElement
	parentHtmlElement.appendChild(table);
}



EightPuzzleUi.prototype.deleteEightPuzzle = function(){
	var parentHtmlElement = this.getParentHtmlElement();
	var eightpuzzleHtmlElement = document.getElementById(this.eightPuzzleId + '_table');
	parentHtmlElement.removeChild( eightpuzzleHtmlElement );
}

EightPuzzleUi.prototype.getParentHtmlElement = function(){
	return document.getElementById(this.eightPuzzleId);
}