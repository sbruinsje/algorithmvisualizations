/*
	This object can perform a Iterative Deepening Search algorithm. It can be used for many  applications. The applications need to:
	- specify the start node
	- specify the goal node
	- Specify where to get the children for a given node.
	- Communicate with this object through SearchNode objects. For example it must return the children for a given node wrapped in a search node object.
	- The data field of the SearchNodes must contain data of a primitive type or contain a .equals method to compare it.
*/
function IterativeDeepening( rootNode, goalNode, getChildren ){
	this.rootNode = rootNode;
	this.goalNode = goalNode;
	this.getChildren = getChildren;
	this.closedList = new Vector();
	this.openList = new Vector();
	this.goalReached = false;
	this.done = false;
	this.maxDepth = 0;
	this.maxDepthIsReached = false;
	this.allMaxDepthNodesVisited = false;
	this.stepNumber = 0;
	this.start();
}

/*
	Start by adding the root node.
*/
IterativeDeepening.prototype.start = function() {
	this.openList.addFirst( this.rootNode );
}

/*
	Perform a step in the algorithm
*/
IterativeDeepening.prototype.step = function(){
	// check if the algorithm is done, if its we can't step() further.
	if( this.done == true ){ 
		return this.closedList;
	} else{
		this.stepNumber+=1;
	
		if( this.allMaxDepthNodesVisited == true ){
			this.maxDepth+=1;
			this.semiReset();
			return this.closedList;
		}
		
		// Get the first node in the open list (=list of nodes to visit in order) and save it as current node.
		var currentNode = this.openList.getFirstElement();
		this.openList.removeFirstElement();

		// Check if the current node is the node we are looking for.
		if(  currentNode.equalData( this.goalNode ) ){
			this.closedList.addElement( currentNode );
			this.goalReached = true;
			this.done = true;
			return this.closedList;
		}

		// Iterative deepening does depth first search up to a cut-off-depth.
		// Check if the node has a depth below the max depth... if it does, we add the children of the current node to the list, otherwise we don't.
		if( currentNode.getDepth() != this.maxDepth ){
			var children = this.getChildren( currentNode );
			for( var i=children.getSize()-1; i>=0; i-- ){
				this.openList.addFirst( children.getElementAt(i) );		// Add all of the current node its children to the front of the open list of nodes.
			}
		} else{
			this.maxDepthIsReached = true;	 // Indicate that the maxDepth has been reached, this is used to check whether it is useful to repeat the search with maxDepth+=1
		}
		
		// Check if there are still nodes to be searched. 
		if( this.openList.getSize() == 0 && this.maxDepthIsReached == true ){ // no nodes to be searched but it might be because of the cutOffDepth.
			this.closedList.addElement( currentNode );
			this.allMaxDepthNodesVisited = true;
			return this.closedList;
		} else if( this.openList.getSize() == 0 && this.maxDepthIsReached == false ){ // no nodes to be searched.. not cause of the cut off depth.
			this.done = true;
		}
		
		this.closedList.addElement( currentNode );
		return this.closedList;
	}
}







/*
	Returns whether the algorithm is finished.
*/
IterativeDeepening.prototype.isDone = function(){
	return this.done;
}

/*
	Resets the values of the Iterative Deepening object, so we can start all over again.
*/
IterativeDeepening.prototype.semiReset = function(){
	this.openList = new Vector();
	this.closedList = new Vector();
	this.maxDepthIsReached = false;
	this.allMaxDepthNodesVisited = false;
	this.done = false;
	this.goalReached = false;
	this.start();
}

/*
	Resets the values of the Iterative Deepening object, so we can start all over again.
*/
IterativeDeepening.prototype.reset = function(){
	this.openList = new Vector();
	this.closedList = new Vector();
	this.maxDepthIsReached = false;
	this.allMaxDepthNodesVisited = false;
	this.done = false;
	this.maxDepth = 0;
	this.stepNumber = 0;
	this.goalReached = false;
	this.start();
}


/*
	Undo a step in the algorithm
*/
IterativeDeepening.prototype.undoStep = function(){
	var stepNumber = this.stepNumber;
	if( stepNumber > 0 ){
		this.reset();
		for( var i=0; i<stepNumber-1; i++ ){
			this.step();
		}
	}
}