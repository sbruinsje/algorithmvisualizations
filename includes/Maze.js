

// ########################################### Maze object ############################################################


function Maze( width, height ){
	if( typeof width == 'undefined' ){ width = 10; }	// default width of a maze (if no width is specified)
	if( typeof height == 'undefined' ){ height = 10; }	// default height of a maze (if no height is specified)
	this.table = new Table( width, height );			// create a table with the given width and height, this will contain all the tiles of the maze.
	this.start = new Coordinate( 0, 0 );				// default start point is in the top left corner of the maze
	this.goal = new Coordinate( width-1, height-1 );	// default goal location is in the bottom right corner of the maze.

	// Initialize the maze by storing a tile in all cells of the maze table.
	for( var y=0; y < height; y++ ){
		for( var x=0; x < width; x++ ){
			this.table.store( x, y, new Tile() );
		}
	}
}

// Define some 'constants'
Maze.prototype.DIRECTIONS = {	'N': new Coordinate(0, -1), 
								'E': new Coordinate(1, 0),
								'S': new Coordinate(0, 1),
								'W': new Coordinate(-1, 0)};

// Gets the width of the maze
Maze.prototype.getWidth = function(){
	return this.table.getNumberOfColumns();
}

// Gets the height of the maze
Maze.prototype.getHeight = function(){
	return this.table.getNumberOfRows();
}

// Gets the number of tiles of the maze
Maze.prototype.getNumberOfTiles = function(){
	return this.getWidth() * this.getHeight();
}


// gets the coordinate that's set as the start of this maze.
Maze.prototype.getStart = function(){
	return this.start;
}


// Sets the start coordinate
Maze.prototype.setStart = function( coord ){
	this.start = coord.clone();
}


// Sets the start coordinate of the maze to a random coordinate.
Maze.prototype.setRandomStart = function(){
	var randX = Math.floor( Math.random()*this.getWidth() );
	var randY = Math.floor( Math.random()*this.getHeight() );
	var randomCoordinate = new Coordinate( randX, randY );

	// Check if the random coordinate is not the same as the goal coordinate. (if the size of the maze is 1 or less then start and goal can be equal.)
	if( this.getGoal().equals( randomCoordinate ) && this.getWidth()*this.getHeight() > 1 ) {
		this.setRandomStart(); // find new start coordinate.
	} else{
		this.setStart( randomCoordinate );
	}
}


// Set random data for a tiles.
Maze.prototype.setCoordinateAsData = function(){
	for( var i=0; i<this.getHeight(); i++ ){
		for( var j=0; j<this.getWidth(); j++ ){
			var coord = new Coordinate(j, i);
			var tile = this.getTile( coord );
			tile.setData( coord );
		}
	}
}


// Checks if a coordinate is the start coordinate
Maze.prototype.isStart = function( coord ){
	var start = this.getStart();
	if( start.equals( coord ) ){
		return true;
	} else{
		return false;
	}
}


// gets the coordinate that's set as the goal of this maze.
Maze.prototype.getGoal = function(){
	return this.goal;
}


// Sets the goal coordinate of the maze
Maze.prototype.setGoal = function( coord ){
	this.goal = coord.clone();
}


// Sets the goal coordinate of the maze to a random coordinate.
Maze.prototype.setRandomGoal = function(){
	var randX = Math.floor( Math.random()*this.getWidth() );
	var randY = Math.floor( Math.random()*this.getHeight() );
	var randomCoordinate = new Coordinate( randX, randY );
	// Check if the random coordinate is not the same as the start coordinate. (if the size of the maze is 1 or less then start and goal can be equal.)
	if( this.getStart().equals( randomCoordinate ) && this.getWidth()*this.getHeight() > 1 ) {
		this.setRandomGoal(); // find new random goal point
	} else{
		this.setGoal( randomCoordinate );
	}
}




// Checks if a coordinate is the goal coordinate
Maze.prototype.isGoal = function( coord ){
	var goal = this.getGoal();
	if( goal.equals( coord ) ){
		return true;
	} else{
		return false;
	}
}


// check if a tile is marked with a specific markType (can be open, visited, stone, but can be anything.)
Maze.prototype.isMarked = function( coord ){
	var tile = this.getTile( coord );
	return tile.isMarked();
}


// Marks a tile with a className, the className can be used for styling or game-object placement (for example: open, visited, stone, etc.)
Maze.prototype.mark = function( coord, markType ){
	var tile = this.getTile( coord );
	tile.mark = markType;
}


// gets the type of mark that this tile has.
Maze.prototype.getMark = function( coord ){
	var tile = this.getTile( coord );
	return tile.getMark();
}

// gets the label from a tile with the specified coordinate
Maze.prototype.getLabel = function( coord ){
	var tile = this.getTile( coord );
	return tile.getLabel();
}


// sets the label of a tile with the specified coordinate
Maze.prototype.setLabel = function( coord, label, applyStyle ){
	if( typeof applyStyle === 'undefined' ){
		applyStyle = null;
	}

	var tile = this.getTile( coord );
	tile.setLabelStyle( applyStyle );
	return tile.setLabel( label );
}


// Checks if the tile with the specified coordinate has a label.
Maze.prototype.hasLabel = function( coord ){
	var tile = this.getTile( coord );
	return tile.hasLabel();
}


// This retrieves the label style that is set for the specified coordinate.
Maze.prototype.getLabelStyle = function( coord ){
	var tile = this.getTile( coord );
	return tile.getLabelStyle();
}



// gets the data from a tile with the specified coordinate
Maze.prototype.getData = function( coord ){
	var tile = this.getTile( coord );
	return tile.getData();
}


// sets the data of a tile with the specified coordinate
Maze.prototype.setData = function( coord, data ){
	var tile = this.getTile( coord );
	return tile.setData( data );
}


// Checks if the tile with the specified coordinate has data stored in it.
Maze.prototype.hasData = function( coord ){
	var tile = this.getTile( coord );
	return tile.hasData();
}


// gets the heuristic from a tile with the specified coordinate
Maze.prototype.getHeuristic = function( coord ){
	var tile = this.getTile( coord );
	return tile.getHeuristic();
}


// sets the heuristic of a tile with the specified coordinate
Maze.prototype.setHeuristic = function( coord, heuristic ){
	var tile = this.getTile( coord );
	return tile.setHeuristic( heuristic );
}


// Checks if the tile with the specified coordinate has a heuristic.
Maze.prototype.hasHeuristic = function( coord ){
	var tile = this.getTile( coord );
	return tile.hasHeuristic();
}




// Fill the tiles of the maze with random data
Maze.prototype.setRandomHeuristics = function(){
	var mazeHeight = this.getHeight();
	var mazeWidth = this.getWidth();
	for( var i=0; i < mazeHeight; i++ ){
		for( var j=0; j < mazeWidth; j++ ){
			//var numberOfTiles = mazeWidth * mazeHeight;
			var heuristic = Math.floor( Math.random()*20 );
			var coord = new Coordinate( j, i );
			this.setHeuristic( coord, heuristic );
		}
	}
}


// Fill the tiles of the maze with random data
Maze.prototype.setManhattanDistance = function(){
	var mazeHeight = this.getHeight();
	var mazeWidth = this.getWidth();
	for( var i=0; i < mazeHeight; i++ ){
		for( var j=0; j < mazeWidth; j++ ){
			var currentX = j;
			var currentY = i;
			var goalX = this.goal.getX();
			var goalY = this.goal.getY();

			var xDistance = (goalX >= currentX) ? (goalX-currentX) : (currentX-goalX);
			var yDistance = (goalY >= currentY) ? (goalY-currentY) : (currentY-goalY);
			var manhattanDistance = xDistance + yDistance;
			var coord = new Coordinate( j, i );
			this.setHeuristic( coord, manhattanDistance );
		}
	}
}


// Deletes all marks
Maze.prototype.deleteMarks = function( coord ){
	for( var i=0; i<this.getHeight(); i++ ){
		for( var j=0; j<this.getWidth(); j++ ){
			var coord = new Coordinate( j, i );
			var tile = this.getTile( coord );
			tile.deleteMark();
		}
	}
}



// Deletes all marks
Maze.prototype.deleteLabels = function( coord ){
	for( var i=0; i<this.getHeight(); i++ ){
		for( var j=0; j<this.getWidth(); j++ ){
			var coord = new Coordinate( j, i );
			var tile = this.getTile( coord );
			tile.deleteLabelStyle();
			tile.deleteLabel();
		}
	}
}


// Places or deletes a wall on one of the sides of a specified tile. 
// 'coord' specifies the coordinate of the tile.
// 'wallSide' specifies which wall. The only valid values are 'N', 'E', 'S' and 'W'
// 'value' specifies whether it will delete or place a wall. Allowed values are 'true' (for placing a wall) or 'false' (for deleting a wall).
Maze.prototype.setWall = function( coord, wallSide, value ){
	var direction; // this will hold the direction of the tile that is adjacent to the wall we are placing.
	var oppositeSide; // this will indicate the opposite side of 'wallSide', we need to this for placing the neighbouring tile's wall.

	switch ( wallSide ){
		case 'N': direction = this.DIRECTIONS['N']; oppositeSide='S'; break;
		case 'E': direction = this.DIRECTIONS['E']; oppositeSide='W';  break;
		case 'S': direction = this.DIRECTIONS['S']; oppositeSide='N';  break;
		case 'W': direction = this.DIRECTIONS['W']; oppositeSide='E';  break;
		default: throw new Error('Invalid paramter value specified for "wallSide", must be N, E, S or W.');
	}

	// set the wall for the tile (specified by coord)
	var tile = this.getTile( coord );
	tile.setWall( wallSide, value );
	
	// Then place the wall for the neighbouring cell aswell.
	var neighbourCoordinate = coord.applyDirection( direction );
	if( this.isValidCoordinate( neighbourCoordinate ) ){
		tile = this.getTile( neighbourCoordinate );
		tile.setWall( oppositeSide, value );
	}
}



// checks whether a tile on the given coordinate has a wall on the specified side.
Maze.prototype.hasWall = function( coord, side ){
	var tile = this.getTile( coord );
	return tile.hasWall( side );
}


// retrieves the tile stored at the specified coordinate.
Maze.prototype.getTile = function( coord ){
	return this.table.retrieve( coord.getY(), coord.getX() );
}



// checks whether the specified coordinate lies within the boundarties of the maze.
Maze.prototype.isValidCoordinate = function( coord ){
	var x = coord.getX();
	var y = coord.getY();
	if( x < 0 ){ 
		return false; 
	} else if( x >= this.getWidth() ){ 
		return false;
	} else if( y < 0 ){ 
		return false;
	} else if( y >= this.getHeight() ){ 
		return false;
	} else{ 
		return true; 
	}
}

// This creates a random perfect maze. A perfect maze is where there is exactly one route from one point to any other point in the graph.
// The algorithm to create it is a depth first search algorithm and comes from http://www.mazeworks.com/mazegen/mazetut/index.htm
Maze.prototype.makeRandomPerfectMaze = function(){
	var width = this.getWidth();						// width (number of columns) of the maze
	var height = this.getHeight();						// height (number of rows) of the maze
	var randomX = Math.floor( Math.random()*width );	// a random x coordinate (between 0 and the width of the maze)
	var randomY = Math.floor( Math.random()*height );	// a random y coordinate (between 0 and the height of the maze)
	var current = new Coordinate( randomX, randomY );	// the coordinate to start the algorithm at is saved as current coordinate.
	var numberOfVisitedCells = 1;						// holds the number of cells that have been visited.
	var totalNumberOfCells = width*height;				// the total number of tiles in the maze.
	var cellStack = new Stack();						// Coordinates which have been traversed are put onto the stack...
														// if the algorithm comes to a death end, it keeps popping values and see if it can go further from that coordinate.


	// This while loop will create the actual maze.
	while( numberOfVisitedCells < totalNumberOfCells ){
		
		// Get a neighbour of the current coordinate, which has all walls in tact 
		var neighbours = this.getNeighbourCoordinates( current );	// get a vector with all neighbours of the coordinate.
		var neighboursWithAllWalls = new Vector();					// will hold all neighbours that have all of its walls in tact.
		
		// check for all neighbours whether they have all walls in tact and store them in a vector: 'neighboursWithAllWalls'
		for( var i=0; i<neighbours.getSize(); i++ ){
			var neighbour = neighbours.getElementAt( i );
			if( this.getTile( neighbour ).hasAllWalls() ){
				neighboursWithAllWalls.addElement( neighbour );
			}
		}
		
		
		if( neighboursWithAllWalls.getSize() > 0 ){ // If there is a neighbour of the current coordinate that has all wals in tact..
			// Pick one of those neighbours at random.
			var randomIndex = Math.floor( Math.random() * neighboursWithAllWalls.getSize() );
			var neighbourWithAllWalls = neighboursWithAllWalls.getElementAt( randomIndex );

			// Break down the wall between those cells
			var direction = this.coordinateIsInDirection( current, neighbourWithAllWalls ); // Find our what direction that neighbour is in (North, east, south or west)
			this.setWall( current, direction, false );
			cellStack.push( current );			// push this coordinate on the stack, so we can come back later to see if we need to break down a wall to another neighbour.
			current = neighbourWithAllWalls;	// Make the neighbour of which we just broke down the wall the current coordinate. We will then start this process all over again with that coordinate.
			numberOfVisitedCells += 1;
		} else{ // if there was no neighbour of the current coordinate which had all walls in tact, pop a value from the stack and make it the current coordinate, then repeat this whole process.
			current = cellStack.pop();
		}
	}
}

// This function checks in what direction 'otherCoord' lies, seen from 'coord'. (so if coord=(3,2) and otherCoord=(4,2) then this will return east.)
Maze.prototype.coordinateIsInDirection = function( coord, otherCoord){
	var coordDifferenceX =  coord.getX() - otherCoord.getX();
	var coordDifferenceY =  coord.getY() - otherCoord.getY();
	
	if( coordDifferenceX >= 1 ){
		return 'W';
	} else if( coordDifferenceX <= -1 ){
		return 'E';
	} else if( coordDifferenceY >= 1 ){
		return 'N';
	} else if( coordDifferenceY <= -1 ){
		return 'S';
	} else{
		throw new Error('Two coordinates that are neighbours of eachother must be passed as arguments.');
	}
}

// returns a Vector with all the neighbour coordinates in it (regardless of whether there is a wall betweeen them).
// A neighbour is defined as all coordinates that are: above, right, under or left of the given coordinate.
Maze.prototype.getNeighbourCoordinates = function( coord ){
	var neighbourCoordinates = new Vector();
	var northCoord = coord.applyDirection( this.DIRECTIONS['N'] );
	var eastCoord = coord.applyDirection( this.DIRECTIONS['E'] );
	var southCoord = coord.applyDirection( this.DIRECTIONS['S'] );
	var westCoord = coord.applyDirection( this.DIRECTIONS['W'] );
	
	if( this.isValidCoordinate(northCoord) ){
		neighbourCoordinates.addElement( northCoord );
	}
	if( this.isValidCoordinate(eastCoord) ){
		neighbourCoordinates.addElement( eastCoord );
	}
	if( this.isValidCoordinate(southCoord) ){
		neighbourCoordinates.addElement( southCoord );
	}
	if( this.isValidCoordinate(westCoord) ){
		neighbourCoordinates.addElement( westCoord );
	}
	
	return neighbourCoordinates;
}


// returns a Vector with all adjacent coordinates that are not seperated by a wall.
// Takes two arguments:
// - 'coord', this is the coordinate for which the adjacent coordinates must be found.
// - 'exclude' [optional] can be used to exclude a adjacent neighbour from the results.
Maze.prototype.getAdjacent = function( coord, exclude ){
	var neighbourCoordinates = new Vector();
	var northCoord = coord.applyDirection( this.DIRECTIONS['N'] );
	var eastCoord = coord.applyDirection( this.DIRECTIONS['E'] );
	var southCoord = coord.applyDirection( this.DIRECTIONS['S'] );
	var westCoord = coord.applyDirection( this.DIRECTIONS['W'] );
	
	if( this.isValidCoordinate(northCoord) && ! this.hasWall( coord, 'N') ){
		neighbourCoordinates.addElement( northCoord );
	}
	if( this.isValidCoordinate(eastCoord) && ! this.hasWall( coord, 'E') ){
		neighbourCoordinates.addElement( eastCoord );
	}
	if( this.isValidCoordinate(southCoord) && ! this.hasWall( coord, 'S') ){
		neighbourCoordinates.addElement( southCoord );
	}
	if( this.isValidCoordinate(westCoord) && ! this.hasWall( coord, 'W') ){
		neighbourCoordinates.addElement( westCoord );
	}
	
	return neighbourCoordinates;
}



// ########################################### Tile object ############################################################
// This object defines a tile in a maze. It can have a wall on any side.
function Tile( northWall, eastWall, southWall, westWall ){
	// Set the default values for the arguments
	this.mark = null;
	this.label = null;
	this.labelStyle = null;
	this.data = null;
	this.heuristic = null;
	if( typeof northWall == 'undefined' ){ northWall = true; }
	if( typeof eastWall == 'undefined' ){ eastWall = true; }
	if( typeof southWall == 'undefined' ){ southWall = true; }
	if( typeof westWall == 'undefined' ){ westWall = true; }
	this.northWall = northWall;
	this.eastWall = eastWall;
	this.southWall = southWall;
	this.westWall = westWall;
}

// This method of the Tile object sets a wall on the specified side.
Tile.prototype.setWall = function( side, value ){
	if( side == 'N' ){
		this.northWall = value;
	} else if( side == 'E' ){
		this.eastWall = value;
	} else if( side == 'S' ){
		this.southWall = value;
	} else if( side == 'W' ){
		this.westWall = value;
	} else{
		throw new Error('invalid parameter value for "side" specified, must be N, E, S or W.');
	}
}



// checks whether this tile has a wall on a specified side (the side parameter must be N, E, S or W.)
Tile.prototype.hasWall = function( side ){
	switch( side ){
		case 'N': return this.northWall; break;
		case 'E': return this.eastWall; break;
		case 'S': return this.southWall; break;
		case 'W': return this.westWall; break;
		default: throw new Error('Invalid parameter value for "side" specified, must be N, E, S or W.');
	}
}


// Checks if this tile has all of its walls.
Tile.prototype.hasAllWalls = function(){
	return (this.northWall == true && this.eastWall == true && this.southWall == true &&  this.westWall == true );
}


// Sets the data field of the tile.
Tile.prototype.setLabelStyle = function( labelStyle ){
	this.labelStyle = labelStyle;
}


// Sets the data field of the tile.
Tile.prototype.getLabelStyle = function(){
	return this.labelStyle;
}


// Sets the data field of the tile.
Tile.prototype.setLabel = function( label ){
	this.label = label;
}


// Returns the value of the data field of the tile.
Tile.prototype.getLabel = function(){
	return this.label;
}


// Checks if the data field contains data.
Tile.prototype.hasLabel = function(){
	return (this.label === null ) ? false : true;
}


// Sets the heuristic field of the tile.
Tile.prototype.setHeuristic = function( heuristic ){
	this.heuristic = heuristic;
}


// Returns the value of the heuristic field of the tile.
Tile.prototype.getHeuristic = function(){
	return this.heuristic;
}


// Checks if the heuristic field contains data.
Tile.prototype.hasHeuristic = function(){
	return (this.heuristic === null ) ? false : true;
}


// gets the data from the tile 
Tile.prototype.getData = function(){
	return this.data;
}


// sets the data of the tile
Tile.prototype.setData = function( data ){
	this.data = data;
}


// Checks if the tile has data stored in it.
Tile.prototype.hasData = function( coord ){
	return (this.data === null ) ? false : true;
}




// Check if the tile is marked with a specific markType ( for example, open, visited, but can be anything... )
Tile.prototype.isMarkedAs = function( markType ){
	if( this.mark === markType ){
		return true;
	} else{ 
		return false;
	}
}


// Check if the tile is marked at all.
Tile.prototype.isMarked = function(){
	if( this.mark === null ){
		false;
	} else{ 
		return true;
	}
}


// gets the type of mark that this tile has.
Tile.prototype.getMark = function(){
	return this.mark;
}


//delete mark that this tile has.
Tile.prototype.deleteMark = function(){
	this.mark = null;
}



//delete label that this tile has.
Tile.prototype.deleteLabel = function(){
	this.label = null;
}




//delete label that this tile has.
Tile.prototype.deleteLabelStyle = function(){
	this.labelStyle = null;
}

// ####################################### Mark Object ###############################################################
// The mark object specifies a mark for a specific tile. For example, you can mark tile (3,4) as 'open' to indicate it is open space.
// The mark constructor takes to arguments:
// - 'coord':		the coordinate of the tile that is being marked.
// - 'className':	the type of mark indicated by a string. The string can be anything (for example 'open', 'stone', 'visited', etc. (this can be used to style the maze).
/*function Mark( coord, className ){
	if( typeof coord === 'undefined' || typeof className === 'undefined' ){
		throw new Error('No coord or className argument supplied!');
	}
	this.coord = coord;
	this.className = className;

}*/