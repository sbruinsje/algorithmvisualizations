// The MazeUi object contains all methods to control the user interface of the maze.
// It takes the following arguments:
// 'mazeId' the id of the maze that this ui belongs to.
// 'maze' this holds the maze object which is being displayed.
// 'gridSize', this determines the size of each tile in the maze. A gridSize of 10 makes tiles of 10x10 pixels.
function MazeUi( mazeId, maze, gridSize ){
	this.maze = maze;
	this.styleManager = new StyleManager();
	this.mazeId = mazeId;
	this.highlightedCells = new Vector(); // this vector holds 'HightlightedCell' objects, to indicate what cells or groups are highlighted.

	// Set the gridSize to the default value if the 'gridSize' argument is not specified, otherwise set it to the specified value.
	this.gridSize = ( typeof gridSize == 'undefined' ) ? 10 : gridSize;
}


// Gets the start coordinate of the maze.
MazeUi.prototype.getStart = function(){
	return this.maze.getStart();
}


// Gets the goal coordinate of the maze.
MazeUi.prototype.getGoal = function(){
	return this.maze.getGoal();
}


MazeUi.prototype.addCellToHighlight = function( coord, group ){
	var cell = new HighlightedCell( coord, group );
	this.highlightedCells.addElement( cell );
}


// This builds the html structure of the maze and outputs it on the screen.
MazeUi.prototype.buildMaze = function(){
	var mazeWidth = this.getMazeWidth();
	var mazeHeight = this.getMazeHeight();
	var parentHtmlElement = this.getParentHtmlElement();
	var table = document.createElement('table');
	table.setAttribute('id',this.mazeId + '_table');
	table.setAttribute('name',this.mazeId + '_table');
	tableHeaderRow = this.makeTableHeaderRow();
	table.appendChild( tableHeaderRow );
	

	// draw the rest of the table.
	for( var i=0; i<mazeHeight; i++ ){
		var row = document.createElement('tr');
		row.setAttribute('id',this.mazeId + '_row_' + i);
		row.setAttribute('name',this.mazeId + '_row_' + i);
		table.appendChild(row);		

		
		// add the row header.
		var rowHeadCell = document.createElement('td');
		var rowHeadCellText = document.createTextNode( String.fromCharCode(65+i) );
		rowHeadCell.style.backgroundColor = 'white';
		rowHeadCell.style.color =  'black';
		rowHeadCell.style.width = this.gridSize + 'px';
		rowHeadCell.style.height = this.gridSize + 'px';
		rowHeadCell.appendChild( rowHeadCellText );
		row.appendChild( rowHeadCell );


		for( var j=0; j<mazeWidth; j++ ){
			var currentCoord = new Coordinate( j, i );
			var cell = document.createElement('td');
			cell.setAttribute('id',this.mazeId + '_cell_' + j + '_' + i);
			cell.setAttribute('name',this.mazeId + '_cell_' + j + '_' + i);
			cell.style.width = this.gridSize + 'px';
			cell.style.height = this.gridSize + 'px';
			
			this.drawWalls( cell, currentCoord );		

			if( this.maze.isMarked( currentCoord ) ){
				var markClassName = this.maze.getMark( currentCoord );
				this.styleManager.addClass( cell, markClassName );
			}

			if( this.getGoal().equals( currentCoord ) ){
				this.styleManager.addClass( cell, 'goal' );
			}

			if( this.getStart().equals( currentCoord ) ){
				this.styleManager.addClass( cell, 'start' );
			}


			row.appendChild(cell);
		}
	}
	parentHtmlElement.appendChild(table);
}


MazeUi.prototype.makeTableHeaderRow = function(){
	// Draw the header row of the table.
	var tableHeadRow = document.createElement('tr');
	for( var j=-1; j<this.getMazeWidth(); j++ ){
		var tableHeadCell = document.createElement('td');
		var tableHeadCellText;
		if( j == -1 ){
			tableHeadCellText = document.createTextNode('');
		} else{
			tableHeadCellText = document.createTextNode(j);
		}
		tableHeadCell.style.backgroundColor = 'white';
		tableHeadCell.style.color =  'black';
		tableHeadCell.style.width = this.gridSize + 'px';
		tableHeadCell.style.height = this.gridSize + 'px';
		tableHeadCell.appendChild(tableHeadCellText);
		tableHeadCell.appendChild( tableHeadCellText );
		tableHeadRow.appendChild( tableHeadCell );
	}

	return tableHeadRow;
}

// This updates the UI to match with the state of the maze object.
MazeUi.prototype.refresh = function(){
	for( var i=0; i<this.getMazeHeight(); i++ ){
		for( var j=0; j<this.getMazeWidth(); j++ ){
			var currentCoord = new Coordinate( j, i );
			var htmlElement = document.getElementById(this.mazeId + '_cell_' + j + '_' + i );
			htmlElement.style.width = this.gridSize + 'px';
			htmlElement.style.height = this.gridSize + 'px';
			
			this.styleManager.setClass( htmlElement, '' );
			
			this.drawLabel( currentCoord );

			this.drawWalls( htmlElement, currentCoord );

			if( this.maze.isMarked( currentCoord ) ){
				var markClassName = this.maze.getMark( currentCoord );
				this.styleManager.addClass( htmlElement, markClassName );
			}

			if( this.getGoal().equals( currentCoord ) ){
				this.styleManager.changeClass( htmlElement, 'goal', 'goal' );
			}

			if( this.getStart().equals( currentCoord ) ){
				this.styleManager.changeClass( htmlElement, 'start', 'start' );
			}
		}
	}

}


MazeUi.prototype.drawLabel = function( coord ){
	this.undrawLabel( coord );
	if( this.maze.hasLabel( coord ) ){
		var htmlElement = document.getElementById(this.mazeId + '_cell_' + coord.getX() + '_' + coord.getY() );
		var label = this.maze.getLabel( coord );
		var labelStyle = this.maze.getLabelStyle( coord );
		var labelSpan = document.createElement('span');
		labelSpan.setAttribute('style', labelStyle );
		var labelTextNode = document.createTextNode( label );
		labelSpan.appendChild( labelTextNode );
		htmlElement.appendChild( labelSpan );
	} 
}


MazeUi.prototype.undrawLabel = function( coord ){
	var htmlElement = document.getElementById(this.mazeId + '_cell_' + coord.getX() + '_' + coord.getY() );
	while( htmlElement.hasChildNodes() ) {
		 htmlElement.removeChild( htmlElement.firstChild );
	}
}

MazeUi.prototype.drawWalls = function( htmlElement, coord ){
	if( this.maze.hasWall( coord, 'N' ) ){
		this.styleManager.addClass( htmlElement, 'hasNorthWall' );
	} else{
		this.styleManager.removeClass( htmlElement, 'hasNorthWall' );
	}
	if( this.maze.hasWall( coord, 'E' ) ){
		this.styleManager.addClass( htmlElement, 'hasEastWall' );
	} else{
		this.styleManager.removeClass( htmlElement, 'hasEastWall' );
	}
	if( this.maze.hasWall( coord, 'S' ) ){
		this.styleManager.addClass( htmlElement, 'hasSouthWall' );
	} else{
		this.styleManager.removeClass( htmlElement, 'hasSouthWall' );
	}
	if( this.maze.hasWall( coord, 'W' ) ){
		this.styleManager.addClass( htmlElement, 'hasWestWall' );
	} else{
		this.styleManager.removeClass( htmlElement, 'hasWestWall' );
	}
}

MazeUi.prototype.deleteMazeUi = function(){
	var parent = this.getParentHtmlElement();
	var mazeTable = document.getElementById(this.mazeId + "_table");
	if( mazeTable == null ){
		return;
	} else if( parent == null ){
		throw new Error("Something went wrong! There seems to be no parent html element for the visualization anymore.");
	} else{
		parent.removeChild( mazeTable );
	}
}


// Get the parentHtmlElement in which the maze is drawn.
MazeUi.prototype.getParentHtmlElement = function(){
	return document.getElementById(this.mazeId);
}


// Get width of the maze
MazeUi.prototype.getMazeWidth = function(){
	return this.maze.getWidth();

}


// Get height of the maze
MazeUi.prototype.getMazeHeight = function(){
	return this.maze.getHeight();
}