/*
	The MazeVisualization object is an which combines several components to form a visualization of how a chosen algorithms search through a maze for a specific goal.
	For example it can show how a Best First Search algorithm searches the maze for the finish from a specified starting point. During this search all changes are displayed in a maze.
	The goal is to show how best first search decides to search through a maze. The mazeVisualization object contains:
	- a 'Maze' object which contains the internal state of a maze.
	- a 'MazeUi' object, which shows an internal state of a maze on the screen.
	- a search algorithm object which contains the state of the search that is being performed.
	- A legenda for the maze that show how different nodes look.
	- The controls of the maze, which are used to control the visualization (performing the algorithm step by step for example).
*/
function MazeVisualization(  visualizationId  ){
	this.visualizationId = visualizationId;	// This is the id of the visualization, it is used to create/identify the html elements and other components of the visualization.
	this.maze;								// This will contain the maze object which holds the internal state of the maze.
	this.mazeUi;							// This will contain the maze Ui object which is used to display the internal state of the maze to the screen.
	this.playSpeed = 1000;					// This determines the speed at which the visualization gets played when using the play button.
	this.gridSize=40;						// Determines the size (width and height) of the grid-squares. The size is in pixels.
	this.mazeWidth=6;						// Determines the number of columns of the maze.
	this.mazeHeight=6;						// Determines the number of rows of the maze.
	this.search;							// Will hold the object of the chosen search algorithm. This is the object that will perform a search on the maze
	this.searchType;						// The type of search that will be performed.
	this.playIntervalHandler;				// This is the handler for ther play interfal. If the visualization is being played automatically, this handler can cancel it.
	this.beamSize = 3;						// This holds the allowed beam size. Only used for the beam search algorithm.
	this.showHeuristicControl = true;		// This determines whether the control for the heuristic should be visible or not.
	this.addControls = true;				// This specify's whether controls are added to the visualization or not.
	this.addLegenda = true;					// This specify's whether a legenda is added to the visualization or not.
	this.startingHeuristic = 'manhattan';	// This holds the heuristic that will be started with whenever the visualization is (re)started.
	this.visualizationHtmlElement = this.getVisualizationHtmlElement();	 // Get HTML element of the visualization container;
	this.mazeHtmlElement;					// Will hold the html element of the maze only (not the legenda or controls).
	this.controlsHtmlElement;				// Will hold the html element of the controls.
	this.legendaHtmlElement;				// Will hold the html element of the legenda.
	this.controlPlay;						// Html element of play button
	this.controlNext;						// Html element of next button
	this.controlPrevious;					// Html element of previous button
	this.controlPause;						// Html element of Pause button
	this.controlReset;						// Html element of reset button
	this.controlHeuristic;					// Html element of heuristic selection list.
	this.controlRandom;						// Html element of randomize button
}

/* PUBLIC METHODS */
/*
	This method sets the search type that will determine which search algorithm is used in the visualization. Argument 'searchType' must be one of the following values: bfs, dfs, id, beam, hc, best, A, Astar, minimax.
*/
MazeVisualization.prototype.selectAlgorithm = function( searchType ){
	switch( searchType ){
		case 'bfs':		this.searchType = 'bfs';		break; 
		case 'dfs':		this.searchType = 'dfs';		break; 
		case 'id':		this.searchType = 'id';			break; 
		case 'beam':	this.searchType = 'beam';		break; 
		case 'hc':		this.searchType = 'hc';			break; 
		case 'best':	this.searchType = 'best';		break; 
		case 'A':		this.searchType = 'A';			break; 
		case 'Astar':	this.searchType = 'Astar';		break; 
		case 'minimax': this.searchType = 'minimax';	break;
		default: throw new Error('invalid search type supplied for method MazeVisualization.prototype.setSearchType(). Allowed values:  bfs, dfs, id, beam, hc, best, A, Astar, minimax.');
	}	
}

/*
	This method starts the visualization with all the settings that have been specified. Chaning settings after this method has been called does not have any effect.
*/
MazeVisualization.prototype.startVisualization = function(){
	if( !this.searchType ){
		throw new Error('A visualization cannot be started without first specifying a search type.');
	}

	// Create legenda or not depending on the settings.
	if( this.addLegenda === true ){
		this.createLegenda();
	}

	this.createMaze(); // display the maze in the html view.
	this.createMazeUi();
	this.setHeuristics();
	

	// Create controls or not depending on the settings.
	if( this.addControls === true ){
		this.createControls();
	}

	this.initSearch();
	this.updateControlAvailability('manual');
	this.refreshMarks(); // Refresh the user interface of the Maze.
}

/*
	This method restarts the visualization with all the settings that have been specified. Chaning settings after this method has been called does not have any effect.
*/
MazeVisualization.prototype.restartVisualization = function(){
	this.deleteVisualizationHtmlElements();
	this.startVisualization();
}

/*
	This method creates the maze (internal state of the maze) that is used in the visualization.
*/
MazeVisualization.prototype.createMaze = function(){
	this.maze = new Maze( this.mazeWidth, this.mazeHeight );
	this.maze.makeRandomPerfectMaze();
	this.maze.setCoordinateAsData(); // this stores the coordinate of the tile as the data of that tile. This is used by the search algorithm.
}

/*
	This method displays the maze that is used in the visualization on the screen.
*/
MazeVisualization.prototype.createMazeUi = function(){
	this.createMazeHtmlElement();
	this.mazeUi = new MazeUi( 'maze_' + this.visualizationId, this.maze, this.gridSize );
	this.mazeUi.buildMaze();
	this.refreshUi();
}

/*
	This sets the default heuristic that is used. Can be either 'random' or 'manhattan'
*/
MazeVisualization.prototype.defaultHeuristic = function( value ){
	this.startingHeuristic = value;
}

/*
	This sets the default heuristic that is used. Can be either 'random' or 'manhattan'
*/
MazeVisualization.prototype.setHeuristics = function(){
	if( this.startingHeuristic == 'manhattan' ){
		this.setManhattanDistance();
	} else{
		this.setRandomHeuristics();
	}
}

/*
	This sets the gridsize of the maze. This is the size in pixels that the grid-squares of the maze will have.
*/
MazeVisualization.prototype.setGridSize = function( value ){
	this.gridSize = value;
}

/*
	This sets the number of columns that the maze will have.
*/
MazeVisualization.prototype.setMazeSize = function( columns, rows ){
	this.mazeWidth = columns;
	this.mazeHeight = rows;
}

/*
	This method sets whether controls should be shown for the visualization. Argument 'value' must be either true of false
*/
MazeVisualization.prototype.showControls = function( value ){
	this.addControls = value;
}

/*
	This method sets whether a legenda should be shown as part of the visualization. Argument 'value' must be either true of false
*/
MazeVisualization.prototype.showLegenda = function( value ){
	this.addLegenda = value;
}

/*
	This method set the speed at which the visualization is played when the play button is used.
*/
MazeVisualization.prototype.setPlaySpeed = function( speed ){
	this.playSpeed = speed;
}

/*
	This method determined whether the heuristic selection list should be visible or not. This list is used to select a heuristic to use for the visualization.
*/
MazeVisualization.prototype.enableHeuristicControl = function( show ){
	this.showHeuristicControl = show;

}

/*
	This method sets the manhattan distance as selected option in the heuristic html selection list. And applies the manhattan distance on the maze so that all grid-squares use that as their heuristic.
*/
MazeVisualization.prototype.setManhattanDistance = function(){
	if( this.controlHeuristic ){
		this.controlHeuristic.selectedIndex = 1;
	}
	this.maze.setManhattanDistance();
}

/*
	This method sets the random  heuristics as selected option in the heuristic html selection list. And applies the random heuristics on the maze so that all grid-squares use that as their heuristic.
*/
MazeVisualization.prototype.setRandomHeuristics = function(){
	if( this.controlHeuristic ){
		this.controlHeuristic.selectedIndex = 2;
	}
	this.maze.setRandomHeuristics();
}

/*
	Sets the allowed beam size. This is only used for the beam search algorithm.
*/
MazeVisualization.prototype.setBeamSize = function( size ){
	this.beamSize = size;
}

/*
	This gets the number of columns of the maze.
*/
MazeVisualization.prototype.getWidth = function(){
	return this.mazeWidth;
}

/*
	This gets the number of rows of the maze.
*/
MazeVisualization.prototype.getHeight = function(){
	return this.mazeHeight;
}


/* ######################################################## Private methods ######################################################## */
/*
	This method updates the mazeUi to reflect any changes in the internal state of the maze.
*/
MazeVisualization.prototype.refreshUi = function(){
	this.mazeUi.refresh();
}

/*
	This method initializes the search algorithm that is to be performed on the maze. Which algorithm will be initialized is specified by the this.searchType property.
	By initializing the search, the searchAlgorithm object is created and knows where to start and what to look for.
*/
MazeVisualization.prototype.initSearch = function(){
	var startCoord = this.maze.getStart();
	var goalCoord = this.maze.getGoal();
	var startNode = new SearchNode( startCoord, startCoord, 0, this.maze.getHeuristic(startCoord) );
	var goalNode = new SearchNode( goalCoord );

	switch( this.searchType ){
		case 'bfs':			this.search = new BreadthFirstSearch( startNode, goalNode, bind( this, this.getMazeChildren ) );
							break;
		case 'dfs':			this.search = new DepthFirstSearch( startNode, goalNode, bind( this, this.getMazeChildren ) ); 
							break;
		case 'id':			this.search = new IterativeDeepening( startNode, goalNode, bind( this, this.getMazeChildren ) ); 
							break;
		case 'beam':		this.search = new BeamSearch( startNode, goalNode, bind( this, this.getMazeChildren ), 4 );
							this.labelHeuristics();
							this.search.beamSize = this.beamSize;
							break;
		case 'hc':			this.search = new BeamSearch( startNode, goalNode, bind( this, this.getMazeChildren ), 1 );
							this.labelHeuristics();
							break;
		case 'best':		this.search = new BeamSearch( startNode, goalNode, bind( this, this.getMazeChildren ), Number.INFINITY );
							this.labelHeuristics();
							break;
		case 'A':			this.search = new A( startNode, goalNode, bind( this, this.getMazeChildren ) );
							this.labelHeuristics();
							this.labelHeuristicsWithDepth('font-size: xx-small;');
							break;
		default:			throw new Error('No searchtype is specified, so no search can be initialized.');
	}
}

/*
	This method refreshes the marks of the nodes in the maze. This needs to be run whenever a node has been visited by the algorithm or whenever any action changes the state of the search that is being performed.
*/
MazeVisualization.prototype.refreshMarks = function(){
	this.maze.deleteMarks();
	this.maze.deleteLabels();
	switch( this.searchType ){
		case 'bfs':		this.markVisitedCoordinates();
						this.labelDepth();
						break;
		case 'dfs':		this.markVisitedCoordinates();
						this.labelDepth();
						break;
		case 'id':		this.markVisitedCoordinates();
						this.labelDepth();
						break;
		case 'hc':		this.markVisitedCoordinates();
						this.labelHeuristics();
						break;
		case 'best':	this.markVisitedCoordinates();
						this.markOpenNodes();
						this.labelHeuristics();
						break;
		case 'beam':	this.markVisitedCoordinates();
						this.markDiscardedNodes();
						this.markOpenNodes();
						this.labelHeuristics();
						break;
		case 'A':		this.markVisitedCoordinates();
						this.markOpenNodes();
						this.labelHeuristics();
						this.labelHeuristicsWithDepth('font-size: xx-small;');
						break;
	}

	// Clear the interval because the algorithm is done.
	if( this.search.isDone() ){
		clearInterval( this.playIntervalHandler );
		if( this.search.goalReached ){
			this.markFinish();
		}
	}
	this.refreshUi();
}


/*
	marks the goal not to indicate the goal has been found.
*/
MazeVisualization.prototype.markFinish = function(){
	var goalCoordinate = this.maze.getGoal();
	this.maze.mark( goalCoordinate, 'finished' );
}

/*
	Visualize all visited coordinates on the maze.
*/
MazeVisualization.prototype.markVisitedCoordinates = function(){
	var visitedCoordinates = this.search.closedList;
	var numberOfVisitedCoordinates = visitedCoordinates.getSize();
	for( var i=0; i<numberOfVisitedCoordinates; i++ ){
		var visitedCoordinate = visitedCoordinates.getElementAt(i).getNode();
		this.maze.mark( visitedCoordinate, 'visited' );
	}
}

/*
	Visualizes the open coordinates on the maze.
*/
MazeVisualization.prototype.markOpenNodes = function(){
	var openList = this.search.openList;
	var numberOfOpenCoordinates = openList.getSize();
	for( var i=0; i<numberOfOpenCoordinates; i++ ){
		var openCoordinate = openList.getElementAt( i ).getNode();
		this.maze.mark( openCoordinate, 'open' );
	}
}

/*
	Visualizes the discarded coordinates on the maze.
*/
MazeVisualization.prototype.markDiscardedNodes = function(){
	if( !this.search.discardedList ){
		throw new Error('The search that is currently being performed does not have a discarded nodes list.');
	}

	var discardedList = this.search.discardedList;
	var numberOfDiscardedCoordinates = discardedList.getSize();
	for( var i=0; i<numberOfDiscardedCoordinates; i++ ){
		var discardedCoordinate = discardedList.getElementAt( i ).getNode();
		this.maze.mark( discardedCoordinate, 'discarded' );
	}
}

/*
	This method labels each cell with the order in which they have been visited.
*/
MazeVisualization.prototype.labelVisitingOrder = function(){
	var visitedCoordinates = this.search.closedList;
	var numberOfVisitedCoordinates = visitedCoordinates.getSize();
	for( var i=0; i<numberOfVisitedCoordinates; i++ ){
		var visitedCoordinate = visitedCoordinates.getElementAt(i).getNode();
		this.maze.setLabel( visitedCoordinate, i );
	}
}


/*
	This method labels each cell with the depth of that grid-square. The depth is the distance to the starting position of the search.	
*/
MazeVisualization.prototype.labelDepth = function( applyStyle ){
	if( typeof applyStyle === 'undefined' ){ applyStyle = null;	}

	var closedList = this.search.closedList;
	for( var i=0; i<closedList.getSize(); i++ ){
		var searchNode = closedList.getElementAt(i);
		var depth = searchNode.getDepth();
		var coord = searchNode.getNode();
		this.maze.setLabel( coord, depth, applyStyle );
	}
}


/* 
	This method labels each cell with the heuristic of that grid-square. 
*/
MazeVisualization.prototype.labelHeuristics = function( applyStyle ){
	if( typeof applyStyle === 'undefined' ){ applyStyle = null;	}

	for( var i=0; i < this.maze.getHeight(); i++ ){
		for( var j=0; j < this.maze.getWidth(); j++ ){
			var coord = new Coordinate( j, i );
			var heuristic = this.maze.getHeuristic( coord );
			this.maze.setLabel( coord, heuristic, applyStyle );			
		}
	}
}


/*
	This method labels each cell with the heuristic and depth of that grid-square in the format: depth + heuristic = new heuristic.
*/
MazeVisualization.prototype.labelHeuristicsWithDepth = function( applyStyle ){
	if( typeof applyStyle === 'undefined' ){ applyStyle = null; }
	
	// Show the heuristics + depth for all closed coordinates.
	var closedList = this.search.closedList;
	var numberOfClosedCoordinates = closedList.getSize();
	for( var i=0; i<numberOfClosedCoordinates; i++ ){
		var closedCoordinate = closedList.getElementAt( i ).getNode();
		var depth = closedList.getElementAt( i ).getDepth();
		var heuristic = this.maze.getHeuristic( closedCoordinate );
		var label = depth + '+' + heuristic + '=' + (heuristic+depth);
		this.maze.setLabel( closedCoordinate, label , applyStyle );
	}

	// show the heuristics + depth for all open coordinates.
	var openList = this.search.openList;
	var numberOfOpenCoordinates = openList.getSize();
	for( var i=0; i<numberOfOpenCoordinates; i++ ){
		var openCoordinate = openList.getElementAt( i ).getNode();
		var depth = openList.getElementAt( i ).getDepth();
		var heuristic = this.maze.getHeuristic( openCoordinate );
		var label = depth + '+' + heuristic + '=' + (heuristic+depth);
		this.maze.setLabel( openCoordinate, label , applyStyle );
	}
}




/*
	This will perform a step in the algorithm and updates the visualization UI to match with the new state.
*/
MazeVisualization.prototype.stepSearch = function(){
	this.search.step();

	// Clear the interval because the algorithm is done.
	if( this.search.isDone() ){
		clearInterval( this.playIntervalHandler );
	}

	this.refreshMarks();
}


/*
	This will undo a step in the algorithm and updates the visualization UI to match with the new state.
*/
MazeVisualization.prototype.undoStepSearch = function(  ){
	this.search.undoStep();
	this.refreshMarks();
}


/*
	This will delete the maze ui. After calling this the maze is not visible on the screen anymore.
*/
MazeVisualization.prototype.deleteMazeUi = function(  ){
	this.mazeUi.deleteMazeUi();
	this.mazeUi = null;
}

/*
	This function gets the children of a given node for the maze.
	This function is given as callback function to the search algorithm, so the search algorithm knows how to get the children of a given node.
*/
MazeVisualization.prototype.getMazeChildren = function( currentNode ){
	var currentDepth = currentNode.getDepth();
	var closedList = this.search.closedList;
	var neighbourCoordinates =  this.maze.getAdjacent( currentNode.getNode() );
	var neighbourSearchNodes = new Vector();

	// remove the coordinates that are in the closed list
	for( var i=0; i<closedList.getSize(); i++ ){
		for( var j=0; j<neighbourCoordinates.getSize(); j++ ){
			var neighbour = neighbourCoordinates.getElementAt( j );
			var closed = closedList.getElementAt( i ).getNode();
			if( neighbour.equals( closed ) ){
				neighbourCoordinates.removeElement( neighbour );
			}
		}
	}
	
	for( var k=0; k<neighbourCoordinates.getSize(); k++ ){
		neighbourCoordinate = neighbourCoordinates.getElementAt( k );
		var heuristic =  this.maze.getHeuristic(neighbourCoordinate);
		var searchNode = new SearchNode( this.maze.getData(neighbourCoordinate), neighbourCoordinate, currentDepth+1, heuristic );
		neighbourSearchNodes.addElement( searchNode );
	}


	return neighbourSearchNodes;
}

/*
	This method creates the legenda of the visualization, which shows how nodes are displayed for all relevant nodes of the current search type. It also creates the html element in which the legenda is shown.
*/
MazeVisualization.prototype.createLegenda = function(){
	this.createLegendaHtmlElement();
	var legenda = document.createElement('img');
	switch( this.searchType ){
		case 'best':
			legenda.setAttribute('src', 'images/maze_legendas/legenda_heuristiek.png');
			break;
		case 'A':
			legenda.setAttribute('src', 'images/maze_legendas/legenda_heuristiek.png');
			break;
		case 'Astar':
			legenda.setAttribute('src', 'images/maze_legendas/legenda_heuristiek.png');
			break;
		case 'beam':
			legenda.setAttribute('src', 'images/maze_legendas/legenda_beam.png');
			break;
		case 'hc':
			legenda.setAttribute('src', 'images/maze_legendas/legenda_hill_climbing.png');
			break; 
		default:	
			legenda.setAttribute('src', 'images/maze_legendas/legenda_geenheuristiek.png');
			break;
	}	
	this.legendaHtmlElement.appendChild( legenda );
}

/* 
	This method checks if a HTML element exists with id 'visualization_<visualizationId>'. If no such html element exists, it is created and appended to the html body element. This html element will contain the whole visualization.
*/
MazeVisualization.prototype.getVisualizationHtmlElement = function(){
	var visualizationHtmlElement = document.getElementById("visualization_" + this.visualizationId );
	if( !visualizationHtmlElement ){
		visualizationHtmlElement = this.createVisualizationHtmlElement();
		var htmlBodyElement = document.getElementsByTagName("body")[0];
		htmlBodyElement.appendChild( visualizationHtmlElement );
	}
	
	return visualizationHtmlElement;
}

/*
	This method creates a HTML which will contain the visualization. It sets the id and name of the element to: 'visualization_<visualizationId>' where <visualizationId> is the visualizationId property of this object.
	This will only succeed if no other element exists with same id.	The newly created element is added to the html body element.
*/
MazeVisualization.prototype.createVisualizationHtmlElement = function(){
	var visualizationHtmlElement = document.getElementById("visualization_" + this.visualizationId );
	if( visualizationHtmlElement ){
		throw new Error('There allready is a html element with the id "visualization_' + this.visualizationId + '", MazeVisualization.prototype.createVisualizationHtmlElement() cannot create an element with the same id.');
	}
	
	visualizationHtmlElement = document.createElement('div');
	visualizationHtmlElement.setAttribute('id','visualization_' + this.visualizationId );
	visualizationHtmlElement.setAttribute('name','visualization_' + this.visualizationId );
	visualizationHtmlElement.setAttribute('class','mazeVisualizationContainer' );

	return visualizationHtmlElement;
}

/*
	This method creates the html element that will hold the maze. It sets the id and name of the element to: 'maze_<visualizationId>' where <visualizationId> is the visualizationId property of this object.
	This will only succeed if no other element exists with same id.	The newly created element is added to the html element of the visualization.
*/
MazeVisualization.prototype.createMazeHtmlElement = function(){
	// Check if there allready is an element with the same id. In that case the element can not be created.
	var mazeHtmlElement = document.getElementById("maze_" + this.visualizationId );
	if( mazeHtmlElement ){
		throw new Error('There allready is a html element with the id "maze_' + this.visualizationId + '", MazeVisualization.prototype.createMazeHtmlElement() cannot create an element with the same id.');
	}
	
	// Create the html element for the maze.
	mazeHtmlElement = document.createElement('div');
	mazeHtmlElement.setAttribute('id', 'maze_' + this.visualizationId );
	mazeHtmlElement.setAttribute('name', 'maze_' + this.visualizationId );
	mazeHtmlElement.setAttribute('class', 'maze mazeVisualization' );
	this.visualizationHtmlElement.appendChild( mazeHtmlElement );
	this.mazeHtmlElement = mazeHtmlElement;
}


/*
	This method creates the html element that will hold the controls of the visualization. It sets the id and name of the element to: 'controls_<visualizationId>' where <visualizationId> is the visualizationId property of this object.
	This will only succeed if no other element exists with same id. The newly created element is added to the html element of the visualization.
*/
MazeVisualization.prototype.createControlsHtmlElement = function(){
	// Check if there allready is an element with the same id. In that case the element can not be created.
	var controlsHtmlElement = document.getElementById("controls_" + this.visualizationId );
	if( controlsHtmlElement ){
		throw new Error('There allready is a html element with the id "controls_' + this.visualizationId + '", MazeVisualization.prototype.createControlsHtmlElement() cannot create an element with the same id.');
	}

	controlsHtmlElement = document.createElement('div');
	controlsHtmlElement.setAttribute('id', 'controls_' + this.visualizationId );
	controlsHtmlElement.setAttribute('name', 'controls_' + this.visualizationId );
	controlsHtmlElement.setAttribute('class', 'controls' );
	this.visualizationHtmlElement.appendChild( controlsHtmlElement );
	
	this.controlsHtmlElement = controlsHtmlElement;
}

/*
	This method creates the html element that will hold the legenda of the visualization. It sets the id and name of the element to: 'legenda_<visualizationId>' where <visualizationId> is the visualizationId property of this object.
	This will only succeed if no other element exists with same id. The newly created element is added to the html element of the visualization.
*/
MazeVisualization.prototype.createLegendaHtmlElement = function(){
	// Check if there allready is an element with the same id. In that case the element can not be created.
	var legendaHtmlElement = document.getElementById("legenda_" + this.visualizationId );
	if( legendaHtmlElement ){
		throw new Error('There allready is a html element with the id "legenda_' + this.visualizationId + '", MazeVisualization.prototype.createLegendaHtmlElement() cannot create an element with the same id.');
	}

	legendaHtmlElement = document.createElement('div');
	legendaHtmlElement.setAttribute('id', 'legenda_' + this.visualizationId );
	legendaHtmlElement.setAttribute('name', 'legenda_' + this.visualizationId );
	legendaHtmlElement.setAttribute('class', 'legenda' );
	this.visualizationHtmlElement.appendChild( legendaHtmlElement );
	
	this.legendaHtmlElement = legendaHtmlElement;
}



/*
	This method deletes the html elements of the visualization except the main container.
*/
MazeVisualization.prototype.deleteVisualizationHtmlElements = function(){
	this.deleteHtmlElement('legenda_' + this.visualizationId );
	this.deleteHtmlElement('controls_' + this.visualizationId );
	this.deleteHtmlElement('maze_' + this.visualizationId );
	this.mazeHtmlElement = null;
	this.controlsHtmlElement = null;
	this.legendaHtmlElement = null;
}

/*
	This method deletes a html element with the given id
*/
MazeVisualization.prototype.deleteHtmlElement = function( elementId ){
	var	element = document.getElementById(elementId);
	if( element ){
		element.parentNode.removeChild( element );
	}
}

/*
	This will start a time to automatically step through the algorithm.
*/
MazeVisualization.prototype.startPlaySearch = function(){
	var bound = bind( this, this.playSearch );
	this.playIntervalHandler = setInterval( bound, this.playSpeed );
	if( this.search.isDone() ){
		this.updateControlAvailability('done');
	} else{
		this.updateControlAvailability('auto');
	}
}

/*
	This will automatically step through the algorithm at a speed that is defined in the this.playSpeed property.
	It sets an interval and stores the handler of it in this.playIntervalHandler, which can be used to cancel it.
*/
MazeVisualization.prototype.playSearch = function(){
	this.stepSearch();
	if( this.search.isDone() ){
		this.updateControlAvailability('done');
	} else{
		this.updateControlAvailability('auto');
	}
}


/*
	This will stop the interval that is performing search steps automatically. Also it will enable the appropriate control buttons.
*/
MazeVisualization.prototype.pauseSearch = function(){
	clearInterval( this.playIntervalHandler );
	if( this.search.isDone() ){
		this.updateControlAvailability('done');
	} else{
		this.updateControlAvailability('manual');
	}
}

/*
	This will reset the search and reflect the changes to the screen.
*/
MazeVisualization.prototype.resetSearch = function(){
	clearInterval( this.playIntervalHandler );
	this.startingHeuristic = (this.controlHeuristic.selectedIndex == 1 ) ? 'manhattan' : 'random';
	this.refreshHeuristics();
	this.search.reset();
	this.refreshMarks();
	if( this.search.isDone() ){
		this.updateControlAvailability('done');
	} else{
		this.updateControlAvailability('manual');
	}
}

/*
	This handles a click on the previous button.
*/
MazeVisualization.prototype.previousSearch = function(){
	this.undoStepSearch();
	if( this.search.isDone() ){
		this.updateControlAvailability('done');
	} else{
		this.updateControlAvailability('manual');
	}
}


/*
	This handles a click on the next button.
*/
MazeVisualization.prototype.nextSearch = function(){
	this.stepSearch();
	if( this.search.isDone() ){
		this.updateControlAvailability('done');
	} else{
		this.updateControlAvailability('manual');
	}
}

/*
	This handles a click on the randomize button.
*/
MazeVisualization.prototype.randomize = function(){
	this.restartVisualization();
}

/*
	This handles a change of the heuristics select box
*/
MazeVisualization.prototype.refreshHeuristics = function(){
	var selectHtmlElement = document.getElementById(this.visualizationId + "_heuristic");
	var selectedValue = selectHtmlElement.options[selectHtmlElement.selectedIndex].value;

	switch( selectedValue ){
		case 'manhattan': 
			this.setManhattanDistance();			
			break;
		case 'random':
			this.setRandomHeuristics();
			break;
		case 'choose':
			break;
		default: throw new Error('This is not a valid heuristic type.');
	}
}

/*
	This function creates the controls that can be used to control the visualization
*/
MazeVisualization.prototype.createControls = function(){
	this.createControlsHtmlElement();

	// create play button
	this.controlPlay = document.createElement("input");
	this.controlPlay.setAttribute("id",this.visualizationId+'_play');
	this.controlPlay.setAttribute("name",this.visualizationId+'_play');
	this.controlPlay.setAttribute("type","button");
	this.controlPlay.setAttribute("value","Play");
	
	// create pause button
	this.controlPause = document.createElement("input");
	this.controlPause.setAttribute("id",this.visualizationId+'_pause');
	this.controlPause.setAttribute("name",this.visualizationId+'_pause');
	this.controlPause.setAttribute("type","button");
	this.controlPause.setAttribute("value","Pauze");

	// create next button
	this.controlNext = document.createElement("input");
	this.controlNext.setAttribute("id",this.visualizationId+'_next');
	this.controlNext.setAttribute("name",this.visualizationId+'_next');
	this.controlNext.setAttribute("type","button");
	this.controlNext.setAttribute("value","Volgende");

	// Create back button
	this.controlPrevious = document.createElement("input");
	this.controlPrevious.setAttribute("id",this.visualizationId+'_previous');
	this.controlPrevious.setAttribute("name",this.visualizationId+'_previous');
	this.controlPrevious.setAttribute("type","button");
	this.controlPrevious.setAttribute("value","Vorige");

	// Create reset button
	this.controlReset = document.createElement("input");
	this.controlReset.setAttribute("id",this.visualizationId+'_reset');
	this.controlReset.setAttribute("name",this.visualizationId+'_reset');
	this.controlReset.setAttribute("type","button");
	this.controlReset.setAttribute("value","Reset");
	

	// create random maze button
	this.controlRandom = document.createElement("input");
	this.controlRandom.setAttribute("id",this.visualizationId+'_randomize');
	this.controlRandom.setAttribute("name",this.visualizationId+'_randomize');
	this.controlRandom.setAttribute("type","button");
	this.controlRandom.setAttribute("value","Randomize");

	// Create heuristics choice list button
	this.controlHeuristic = document.createElement("select");
	this.controlHeuristic.setAttribute("id",this.visualizationId+'_heuristic');
	this.controlHeuristic.setAttribute("name",this.visualizationId+'_heuristic');
	
	/* OPTION 1 */
	option = document.createElement("option");
	option.setAttribute("id",this.visualizationId + "_heuristic_title");
	option.setAttribute("value","choose");
	var optionText = document.createTextNode('--Kies heuristiek--');
	option.appendChild( optionText );
	this.controlHeuristic.appendChild(option);
	/* OPTION 2 */
	option = document.createElement("option");
	option.setAttribute("id",this.visualizationId + "_heuristic_manhattan");
	option.setAttribute("value","manhattan");
	if( this.startingHeuristic == 'manhattan' ){
		option.setAttribute("selected","selected");
	}
	var optionText = document.createTextNode('Manhattan Heuristic');
	option.appendChild( optionText );
	this.controlHeuristic.appendChild(option);
	/* OPTION 2 */
	option = document.createElement("option");
	option.setAttribute("id",this.visualizationId + "_heuristic_random");
	option.setAttribute("value","random");
	var optionText = document.createTextNode('Random Heuristic');
	if( this.startingHeuristic == 'random' ){
		option.setAttribute("selected","selected");
	}
	option.appendChild( optionText );
	this.controlHeuristic.appendChild(option);
	
	if( this.showHeuristicControl == false ){
		this.controlHeuristic.style.display = "none";
	}

	// append the controls to their container and the container to the parent element.
	this.controlsHtmlElement.appendChild(this.controlPrevious);
	this.controlsHtmlElement.appendChild(this.controlNext);
	this.controlsHtmlElement.appendChild(this.controlPause);
	this.controlsHtmlElement.appendChild(this.controlPlay);
	this.controlsHtmlElement.appendChild(this.controlReset);
	this.controlsHtmlElement.appendChild(this.controlRandom);
	this.controlsHtmlElement.appendChild(this.controlHeuristic);

	// Set event handlers for the controls.
	this.controlPrevious.onclick = bind(this, this.previousSearch );
	this.controlNext.onclick = bind(this, this.nextSearch );
	this.controlPlay.onclick = bind(this, this.startPlaySearch );
	this.controlPause.onclick = bind(this, this.pauseSearch );
	this.controlReset.onclick = bind(this, this.resetSearch );
	this.controlRandom.onclick = bind(this, this.randomize );
	this.controlHeuristic.onchange = bind(this, this.resetSearch );
}

/*
	This will enable the right controls considering the supplied argument 'state'. Valid 'state' values are: manual, auto
*/
MazeVisualization.prototype.updateControlAvailability = function( newState ){
	if( typeof newState == 'undefined' ){
		throw new Error('control availability can not be determined without supplying a state.');
	}
	
	if( newState == 'done' ){
		this.controlPause.disabled = true;
		this.controlReset.disabled = false;
		this.controlPlay.disabled = true;
		this.controlNext.disabled = true;
		this.controlPrevious.disabled = false;
		return;
	} else if( newState == 'auto' ){
		this.controlPause.disabled = false;
		this.controlReset.disabled = false;
		this.controlPlay.disabled = true;
		this.controlNext.disabled = true;
		this.controlPrevious.disabled = true;
		return;
	} else if( newState == 'manual' ){
		this.controlPause.disabled = true;
		this.controlReset.disabled = ( this.search.stepNumber == 0 ) ? true : false;
		this.controlPlay.disabled = false;
		this.controlNext.disabled = false;
		this.controlPrevious.disabled = ( this.search.stepNumber == 0 ) ? true : false;
		return;
	}
}