function Minimax( rootNode, getChildren ){
	this.rootNode = rootNode;
	this.getChildren = getChildren;
	this.done = false;
	this.stepNumber = 0;
	this.maxStepNumber = 0;
}





Minimax.prototype.step = function(){
	// check if the algorithm is done, if its we can't step() further.
	if( this.done == true ){ 
		return this.closedList;
	}
	
	this.maxStepNumber += 1;
	this.stepNumber = 0;

	var currentNode = this.rootNode;
	var maxMove = this.maxMove( currentNode );
	if( this.stepNumber >= this.maxStepNumber ){
		return;
	}
	currentNode.getNode().setHeuristic( maxMove );
	this.stepNumber+=1;
	this.done = true;
	
	
}

Minimax.prototype.maxMove = function( currentNode ){
	var children = this.getChildren( currentNode );
	if( children.getSize() == 0 ){
		return currentNode.getNode().getHeuristic();
	}


	var bestMove = null;
	for( var i=0; i<children.getSize(); i++ ){
		var children = this.getChildren( currentNode );
		var move = this.minMove( children.getElementAt( i ) );
		
		if( this.stepNumber >= this.maxStepNumber ){
			return;
		}
		
		if( move > bestMove || bestMove == null ){
			bestMove = move;
		}
	}
	currentNode.getNode().setHeuristic( bestMove );
	this.stepNumber+=1;
	return bestMove;
}


Minimax.prototype.minMove = function( currentNode ){
	var children = this.getChildren( currentNode );
	if( children.getSize() == 0 ){
		return currentNode.getNode().getHeuristic();
	}

	var bestMove = null;
	for( var i=0; i<children.getSize(); i++ ){
		var children = this.getChildren( currentNode );
		var move = this.maxMove( children.getElementAt( i ) );
		if( this.stepNumber >= this.maxStepNumber ){
			return;
		}
		if( move < bestMove || bestMove == null ){
			bestMove = move;
		}
	}

	currentNode.getNode().setHeuristic( bestMove );
	this.stepNumber+=1;
	return bestMove;
}





// Returns whether the algorithm is finnished.
Minimax.prototype.isDone = function(){
	return this.done;
}



Minimax.prototype.partialReset = function(){
	this.done = false;
	this.stepNumber = 0;
	this.resetHeuristics( this.rootNode );
}


// Resets the values of the BreadthFirstSearch object, so we can start all over again.
Minimax.prototype.reset = function(){
	this.done = false;
	this.stepNumber = 0;
	this.maxStepNumber = 0;
	this.resetHeuristics( this.rootNode );
}

Minimax.prototype.resetHeuristics = function( searchNode ){
	var children = this.getChildren( searchNode );
	if( children.getSize() == 0 ){
		return;
	}
	
	searchNode.getNode().setHeuristic( null );

	for( var i=0; i<children.getSize(); i++ ){
		var child = children.getElementAt(i);
		this.resetHeuristics( child );
	}
	
	return;
}


Minimax.prototype.undoStep = function(){
	if( this.stepNumber > 0 ){
		this.maxStepNumber -= 2;
		this.partialReset();
		this.step();
	}
}