function SearchNode( data, node, depth, heuristic ){
	this.node = node;
	this.data = data;
	this.heuristic = heuristic;
	this.depth = depth;
}

SearchNode.prototype.getData = function(){
	return this.data;
}

SearchNode.prototype.getNode = function(){
	return this.node;
}


SearchNode.prototype.getDepth = function(){
	return this.depth;
}


SearchNode.prototype.getHeuristic = function(){
	return this.heuristic;
}


SearchNode.prototype.equals = function( otherNode ){
	if( ! this.equalData( otherNode ) ){
		return false;
	} else if( !this.equalNode( otherNode ) ){
		return false;
	} else if( this.heuristic != otherNode.heuristic ){
		return false;
	} else if( this.depth != otherNode.depth ){
		return false;
	} else{
		return true;
	}
}


SearchNode.prototype.equalNode = function( otherNode ){
	var otherNode = otherNode.getData();
	if( typeof this.node == 'number' && typeof otherNode == 'number' ){
		return this.node == otherNode;
	} else if( typeof this.node == 'string' && typeof otherNode == 'string' ){
		return this.node == otherNode;
	} else if( typeof this.node == 'boolean' && typeof otherNode == 'boolean' ){
		return this.node == otherNode;
	} else if( typeof this.node == 'object' && typeof otherNode == 'object' ){
		if( this.node == null && otherNod == null ){
			return true;
		} else if( this.node == null || otherNode == null ){
			return false;
		} else{
			return this.node.equals( otherNode );
		}
    } else{
		return false;
	}
}


SearchNode.prototype.equalData = function( otherNode ){
	var otherData = otherNode.getData();
	if( typeof this.data == 'number' && typeof otherData == 'number' ){
		return this.data == otherData;
	} else if( typeof this.data == 'string' && typeof otherData == 'string' ){
		return this.data == otherData;
	} else if( typeof this.data == 'boolean' && typeof otherData == 'boolean' ){
		return this.data == otherData;
	} else if( typeof this.data == 'object' && typeof otherData == 'object' ){
		if( this.data == null && otherData == null ){
			return true;
		} else if( this.data == null || otherData == null ){
			return false;
		} else{
			return this.data.equals( otherData );
		}
    } else{
		return false;
	}
}


// THIS WORKS ONLY FOR SEARCHNODES WHO'S node FIELD VALUE HAS A CLONE METHOD AS WELL AND WHERE THE DATA IS OF A PRIMITIVE TYPE.
SearchNode.prototype.clone = function(){
	var clone = new SearchNode( this.data, this.node.clone(), this.depth, this.heuristic );
	return clone;

}
