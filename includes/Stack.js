function Stack(){

	this.vector = new Vector();
	

	// Gets the number of elements on the stack.
	this.getSize = function(){
		return this.vector.getSize();
	}

	// Checks whether the stack is empty.
	this.isEmpty = function(){
		return this.vector.isEmpty();
	}


	// puts an element on top of the stack.
	this.push = function( el ){
		this.vector.addElement( el );
		return this;
	}



	// removes the top element from the stack and returns it.
	this.pop = function(){
		if( this.isEmpty() ){
			throw new Error("pop() was called on an empty stack.");
		}
		var popValue = this.top();
		this.vector.removeLastElement();
		return popValue;
	}


	// returns the top value of the stack
	this.top = function(){
		if( this.isEmpty() ){
			throw new Error("top() was called on an empty stack.");
		}

		return this.vector.getLastElement();
	}


	// returns a clone of the stack.
	this.clone = function(){
		var clone = new Stack();
		clone.vector = this.vector.clone();
		return clone;
	}
}