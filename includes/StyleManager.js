function StyleManager(){
}

// This methodes removes the class specified by removeClass in the classString.
StyleManager.prototype.removeClass = function( htmlElement, removeClass ){
	var classVector = this.getClasses(htmlElement);
	if( classVector.contains( removeClass ) ){
		classVector.removeElement( removeClass );
	}
	htmlElement.className = this.classVectorToString(classVector);
}

// This method checks whether the given htmlElement has a class set in its className property.
StyleManager.prototype.hasClass = function( htmlElement, className ){
	var classVector = this.getClasses(htmlElement);
	
	if( classVector.contains( className ) ){
		return true;
	} else{
		return false;
	}
}

// This method adds a class to the htmlElement
StyleManager.prototype.addClass = function ( htmlElement, addClass ){
	var classVector = this.getClasses(htmlElement);
	
	if( this.hasClass( htmlElement, addClass ) ){
		return; // no need to add it because the htmlElement allready has that class.
	} else{
		classVector.addElement( addClass );
		htmlElement.className = this.classVectorToString(classVector);
	}
}



// This method sets the className property for the htmlElement to 'setClass'
StyleManager.prototype.setClass = function ( htmlElement, setClass ){
	htmlElement.className = setClass.trim();
}


// This method replaces the class specified by 'replaceThis' with 'replaceWithThis' or just adds 'replaceWithThis' if the 'replaceThis' class doesnt exist.
StyleManager.prototype.changeClass = function( htmlElement, replaceThisClass, replaceWithThisClass ){
	var classVector = this.getClasses(htmlElement);
	
	if( classVector.contains(replaceThisClass) ){
		classVector.removeElement(replaceThisClass);
		classVector.addElement( replaceWithThisClass );
	} else{
		classVector.addElement( replaceWithThisClass );
	}

	htmlElement.className = this.classVectorToString(classVector);
}


// this method gets a vector with the classes that are set in the htmlElement's className property.
StyleManager.prototype.getClasses = function( htmlElement ){
	var classString = htmlElement.className;
	var classArray = classString.split(' ');
	var classVector = new Vector();
	for( var i=0; i<classArray.length; i++ ){
		classVector.addElement( classArray[i] );
	}

	return classVector;
}


// This method changes a vector with strings to one string where each vector element is seperated by a space.
StyleManager.prototype.classVectorToString = function( classVector ){
	var classString = '';
	for( var i = 0; i<classVector.getSize(); i++ ){
		classString += ' ' + classVector.getElementAt(i);
	}
	return classString.trim();
}


/*

StyleManager.prototype.resetClass = function( htmlElement, className ){
	if( this.hasClass( htmlElement, className ) ){
		this.removeClass( htmlElement, className );
	} 
	
	this.addClass( htmlElement, className );
}*/