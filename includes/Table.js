
// ############################################ Table class #########################################################
function Table( numberOfRows, numberOfColumns ){
	this.table = new Vector(); // will hold the table, its a vector where each element represents a row. Each element of the vector is itsself a vector, which represents the columns/cells.
	
	// call the construct table, to create the table and add rows to it.
	this.constructTable( numberOfRows, numberOfColumns );
}



// Returns the number of rows in the table.
Table.prototype.getNumberOfRows = function(){
	return this.table.getSize();
}


// Returns the number of columns of the table.
Table.prototype.getNumberOfColumns = function(){
	if( this.getNumberOfRows() == 0 ){
		return 0; // no rows exist, so no columns exist either.
	} else{
		// look at a row, and check how many cells it has.
		var row = this.getRow( 0 );
		return row.getNumberOfCells();
	}
}

// Saves the given data into the specified cell.
Table.prototype.store = function( rowIndex, columnIndex, data ){
	var cell = this.getCell( rowIndex, columnIndex );
	cell.store( data );
	return this;
}

// Retrieves data from the specified cell.
Table.prototype.retrieve = function( rowIndex, columnIndex ){
	var cell = this.getCell( rowIndex, columnIndex );
	return cell.retrieve();
}


// Get's a row at a given index.
Table.prototype.getRow = function( rowIndex ){
	// Check if the row index is valid.
	if( rowIndex > this.getNumberOfRows()-1 || rowIndex < 0 ){
		throw new Error("Can not get the row at the given row index. The index is invalid.");
	}
	return this.table.getElementAt( rowIndex );
}



// Gets the cell at given row/column
Table.prototype.getCell = function( rowIndex, columnIndex ){
	var row =  this.getRow( rowIndex );
	return row.getCell( columnIndex );
}

// Add a given ammount of rows with a given ammount of cells to the end of the table.
Table.prototype.addRows = function( numberOfRows, numberOfColumns ){
	for( var i = 0; i < numberOfRows; i++ ){
		this.addRow( numberOfColumns );
	}

	return this;
}

// Add an empty row to the table.
Table.prototype.addRow = function( numberOfColumns ){
	// If a row allready exist, only rows with the same number of columns as that row can be added.
	if( this.getNumberOfRows() != 0 ){
		numberOfColumns = this.getNumberOfColumns();
	} else if( typeof numberOfColumns == 'undefined' ){
		numberOfColumns = 1;
	}

	var newRow = new Row( numberOfColumns );
	this.table.addElement( newRow );
	return this;
}

// Add a given ammount of columns to the table.
Table.prototype.addColumns = function( numberOfColumns ){
	for( var i=0; i < numberOfColumns; i++ ){
		this.addColumn();
	}

	return this;
}

// Add an empty column to the table.
Table.prototype.addColumn = function(){
	// If no rows exist, create a row with one cell. This is the added column.
	if( this.getNumberOfRows() == 0 ){
		this.addRow( 1 );
		return this;
	} else{
		// Add an empty cell to the end of all rows.
		for( var i=0; i < this.getNumberOfRows(); i++ ){
			var row = this.getRow( i );
			row.addCell();
		}
		return this;
	}
}

/*
// This resizes the table to a specific number of rows and columns.
// If the table is made smaller in width or height, some data may get erased.
Table.prototype.resize = function( numberOfRows, numberOfColumns ){
	// resize the table to the correct number of rows. 
	while( this.table.getSize() != numberOfRows ){
		if( this.table.getSize() < numberOfRows ){ // if the table is smaller than requested.
			this.table.addRow( numberOfColumns ); // add a row.
		} else{ // if table is bigger than requested.
			this.table.removeLastElement(); // remove a row
		}
		
		// resize the table to the correct number of columns.
		if( this.table.getNumberOfColumns < numberOfColumns ){
			// add column
		} else{
			while(
			this.table.
		
	}
}*/

Table.prototype.makeEmpty = function(){
	this.table = new Vector();
	return this;
}

/*
// This checks whether this table is the same as the other table.
Table.prototype.equals = function( otherTable ){
	// If the supplied otherTable argument is null, the values are not equal, so return false;
	if( otherTable == null ){
		return false;
	}

	var numRowsThis = this.getNumberOfRows();
	var numRowsOther = otherTable.getNumberOfRows();
	var numColsThis = this.getNumberOfColumns();
	var numColumnsOther = otherTable.getNumberOfColumns();
	
	// Check if the size of the table is the same, if its not they are not equal for sure...
	if( numRowsThis != numRowsOther || numColumnsThis != numColumnsOther ){
		return false;
	}
	
	// The size of the table is equal, now check whether the contents of the cells is equal.
	for( var i=0; i<numRowsThis; i++ ){
		for( var j=0; j<this.getNumberOfColumns(); j++ ){
			var cellContentsThis = this.retrieve( i, j );
			var cellContentsOther = otherTable.retrieve( i, j );
			
			if( typeof cellContentsThis == "object" ){
				var instanceThis = cellContentsThis
				if( cellContentsThis == null && cellContentsOther == null ){
					return false;
				} else{
					return;
			}
		}
	}
}*/


// Creates an empty table with 'this.numRows' rows and 'this.numCols' columns.
// If a table was allready created the old table will be replaced by the one.
Table.prototype.constructTable = function( numberOfRows, numberOfColumns ){
	this.table = new Vector();	// overwrite the old table with an empty vector, to which rows can be added.
	
	if( typeof numberOfRows == 'undefined' || typeof numberOfColumns == 'undefined' ){		
		return; // no rows or columns need to be added, the table is now an empty table.
	}
	
	
	// Create the given ammount of rows.
	this.addRows( numberOfRows, numberOfColumns );
	
	return this;
}



// ####################################### Row class: is used for the rows in a table. ############################################
function Row( numberOfCells ){
	this.row = new Vector();
	this.constructRow( numberOfCells );
}
	
// Gets the number of cells in the row.
Row.prototype.getNumberOfCells = function(){
	return this.row.getSize();
}


// Get cell
Row.prototype.getCell = function( index ){
	if( index > this.getNumberOfCells()-1 ){
		throw new Error("Can not get the cell at the given index. The index is invalid.");
	}

	return this.row.getElementAt( index );
}


// Add an empty cell to the end of the row.
Row.prototype.addCell = function(){
	var newCell = new Cell( null );
	this.row.addElement( newCell );
	return newCell;
}


// Constructs the row and adds the given number of cells to it.
Row.prototype.constructRow = function( numberOfCells ){
	if( numberOfCells <= 0 ){
		throw new Error("Can not create a row with less than one cell.");
	}
	this.row = new Vector();
	
	// add the given number of cells to the row.
	for( var i=0; i < numberOfCells; i++ ){
		this.addCell();
	}
}



// ####################################### Cell class: will be used as cells in the table (rows). ############################################
function Cell( cellData ){
	if( typeof cellData == 'undefined' ){
		cellData = null;
	}
	this.cellData = cellData;
}


// retrieves the data stored in this cell.
Cell.prototype.retrieve = function(){
	return this.cellData;
}


// Store data
Cell.prototype.store = function( data ){
	this.cellData = data;
	return this;
}