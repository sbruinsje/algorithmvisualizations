// This object is both a Node and an entire tree structure at the same time. 
// It is a node in the sense that it holds data and has an idea.
// It is a tree in the sense that it has a reference to its children and has operations that affects other nodes like its children and their children etc.

// The constructor takes 5 arguments: 
// - id: which is used to identify the node
// - data: which is the data stored in the node, this can be anything.
// - label: some text or name that is associated with the node.
// - children: vector containing the children
function TreeNode( nodeId, data, label, children ){
	this.nodeId = nodeId;
	this.data = data;
	this.label = label;
	if( typeof children == 'undefined' ) {
		this.children = new Vector();
	} else{
		this.children = children;
	}
}



// Returns a copy of the tree. WORKS ONLY PROPERLY FOR TREES THAT STORE DATA OF A PRIMITIVE TYPE
TreeNode.prototype.clone = function(){
	var clonedChildren = new Vector();
	for( var i=0; i<this.getNumberOfChildren(); i++ ){
		var child = this.getChild( i );
		clonedChildren.addElement( child.clone() );
	}

	var clone = new TreeNode( this.nodeId, this.data, this.label, clonedChildren );

	return clone;
}




TreeNode.prototype.findNode = function( data ){
	if( typeof this.data == 'number' && typeof otherData == 'number' ){
		if( this.data == data ){
			return this;
		}
	} else if( typeof this.data == 'string' && typeof otherData == 'string' ){
		if( this.data == data ){
			return this;
		}
	} else if( typeof this.data == 'boolean' && typeof otherData == 'boolean' ){
		if( this.data == data ){
			return this;
		}
	} else if( typeof this.data == 'object' && typeof otherData == 'object' ){
		if( this.data == null && data == null ){
			return this;
		} else if( this.data == null || otherData == null ){
			return -1;
		} else{
			if( this.data.equals( data ) ){
				return this;
			}
		}
    } else{
		return -1;
	}
}



// Get's a node (=same as subtree) with the given nodeId
TreeNode.prototype.getSubtree = function( nodeId ){
	if( this.nodeId == nodeId ){
		return this;
	}
	
	for( var i=0; i<this.getNumberOfChildren(); i++ ){
		var child = this.getChild( i );
		var result = child.getSubtree( nodeId );
		if( result != false ){
			return result;
		}
	}
	return false;
}



// Add's a node (=same as subtree) to another node.
TreeNode.prototype.addSubtree = function( addNode ){
	
	if( addNode.hasParent() ){
		var index = addNode.parent.hasChildAtIndex( addNode );
		if( index != -1 ){
			addNode.parent.removeChild( addNode );
		}
	}

	addNode.parent = this;
	this.addChild( addNode );
}


TreeNode.prototype.hasChildAtIndex = function( searchChild ){
	for( var i=0; i<this.getNumberOfChildren(); i++ ){
		var child = this.getChild( i );
		if( child == searchChild ){
			return i;
		} else{
			return -1;
		}
	}
}







// Returns the depth of the node in the tree.
TreeNode.prototype.getDepth = function(){
	if( this.parent == null ){
		return 0;
	}
	return this.parent.getDepth() + 1;
}


// Sets what its parent node is (can be used to move it onto another tree.
TreeNode.prototype.setParent = function( node ){
	this.parent = node;
}


// Gets the parent of the tree.
TreeNode.prototype.getParent = function(){
	return this.parent;
}

// Checks if this node has a parent.
TreeNode.prototype.hasParent = function(){
	if( this.parent == null ){
		return false;
	} else{
		return true;
	}
}


// Gets a child at a given index.
TreeNode.prototype.getChild = function( i ){
	if( i > this.getNumberOfChildren()-1 || i < 0 ){
		throw new Error("There is no child node with the given index.");
	} else{
		return this.children.getElementAt( i );
	}
}


// adds a child to the specified node.
TreeNode.prototype.addChild = function( node, i ){
	node.setParent( this );

	if( typeof i == 'undefined' ){
		this.children.addElement( node );
	} else if( i > this.getNumberOfChildren() || i < 0 ){
		throw new Error('Invalid index to add a child at.');
	} else{
		this.children.insertElementAt( i );
	}		
}


// removes a child at the given index.
TreeNode.prototype.removeChild = function( i ){
	if( i >= this.getNumberOfChildren() || i<0 ){
		throw new Error("Can't remove a child at the given index. The index is invalid.");
	} else{
		this.children.removeElementAt( i );
	}
}


// Returns the size of the tree.
TreeNode.prototype.getTreeSize = function(){
	var size = 0;
	var numberOfChildren = this.getNumberOfChildren();
	
	if( numberOfChildren <= 0 ){
		return 1; // the size of this tree is 1 because it consists of only this (root) node, it doesn't have children.
	} else{
		size+=1; // this node itsself.
		for( var i=0; i < numberOfChildren; i++ ){ // + the size of all its children nodes/trees
			var child = this.children.getElementAt(i);
			size += child.getTreeSize();
		}
		return size;
	}
}



// Returns the id of the node.
TreeNode.prototype.getId = function(){
	return this.nodeId;
}

// Returns the label of the node.
TreeNode.prototype.getLabel = function(){
	return this.label;
}


// Returns the label of the node.
TreeNode.prototype.setLabel = function( label ){
	return this.label = label;
}


// Returns the label of the node.
TreeNode.prototype.removeLabel = function(){
	return this.label = null;
}


// Returns the label of the node.
TreeNode.prototype.hasLabel = function(){
	return (this.label == null) ? false : true;
}

// Returns the data of the node.
TreeNode.prototype.getData = function(){
	return this.data;
}


// Returns the value of the node.
TreeNode.prototype.getValue = function(){
	return this.data.value;
}


// sets the heuristic of the node.
TreeNode.prototype.setHeuristic = function( value ){
	return this.data.heuristic = value;
}

// Returns the heuristic of the node.
TreeNode.prototype.getHeuristic = function(){
	return this.data.heuristic;
}

// Returns the data of the node.
TreeNode.prototype.setNodeClass = function( value ){
	this.data.nodeClass = value;
}

// Returns the data of the node.
TreeNode.prototype.setAsGoal = function(){
	this.data.isGoal = true;
}

// Returns the data of the node.
TreeNode.prototype.deleteNodeClass = function(){
	this.data.nodeClass = null;
}

// Returns the children of the node.
TreeNode.prototype.getChildren = function(){
	return this.children;
}


// returns how many children this node has.
TreeNode.prototype.getNumberOfChildren = function(){
	return this.children.getSize();
}


// Returns whether this node has any children at all.
TreeNode.prototype.hasChildren = function(){
	return (this.getNumberOfChildren() == 0 ) ? false : true;
}






// Checks whether this tree is equal to some other tree.
// !!!!!!!!!!!!!!!!!!!!!! NOT FINNISHED AND OR TESTED YET!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
TreeNode.prototype.equals = function( otherNode ){
	var otherData = otherNode.getData();
	
	if( typeof otherData == 'object' ){
		return this.data.equals( otherData );
	} else{
		return otherData == this.data;
	}
}

