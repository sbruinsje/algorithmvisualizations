/*
	The TreeVisualization object is an which combines several components to form a visualization of how a chosen algorithms searches through a tree for a specific goal.
	For example it can show how a Best First Search algorithm searches the tree for a specified goal node. During this search all changes are displayed in the tree.
	The goal is to show how best first search decides to search through a tree. The TreeVisualization object contains:
	- a 'TreeNode' object which contains the internal state of a tree.
	- a 'TreeUi' object, which shows an internal state of a tree on the screen.
	- a search algorithm object which contains the state of the search that is being performed.
	- A legenda for the tree that show how different nodes look.
	- The controls of the tree, which are used to control the visualization (performing the algorithm step by step for example).
*/
function TreeVisualization( visualizationId ){
	if( typeof visualizationId == 'undefined' ){ 	
		throw new Error('No id for the tree visualization specified!'); 
	}
	this.visualizationId = visualizationId;	// Identify's the tree, this must be the name of the html element that will contain it.
	this.playIntervalHandler = null;		// The handler of the interval-timer that plays the visualization. (used for cancelling the time.)
	this.tree = null;						// The tree that is used for the visualization
	this.spacetreeUi = null;				// The tree UI that is used for displaying the tree of the visualization.
	this.search = null;						// The search algorithm object that is being performed.
	this.playSpeed = 1000;					// The speed at which the visualization is played.
	this.goal = null;						// The goal-data that is search for in the tree.
	this.beamSize = 3;						// The size of the beam if BeamSearch is used.
	this.randomHeuristics = false;			// This specify's whether random heuristics should be generated for the tree at the start of the visualization.
	this.heuristicAsLabel = true;			// This specify's whether heuristics are used as label of the nodes or not.
	this.addControls = true;				// This specify's whether controls are added to the visualization or not.
	this.addLegenda = true;					// This specify's whether a legenda is added to the visualization or not.
	this.allowDragging = this.detectAllowDragging();	// This specify's whether dragging the tree around its parent HTML element is allowed. (is useful for large trees that do not fit on the screen.)
	this.addLabels = false;					// This specifies whether the labels of the nodes of the tree are displayed or not.
	this.offsetX = 0;						// This defines the horizontal distance between the middle of the parent html element and the root node.
	this.offsetY = 100;						// This defines the vertical distance between the middle of the parent html element and the root node.
	this.isMultiplayerTree = false;			// Defines whether the tree is a multiplayer tree where each level corresponds to a different player.
	this.nodeWidth = 60;					// Defines the width of a node of the tree.
	this.nodeHeight = 20;					// Defines the height of a node of the tree.
	this.nodeClassName = '';				// This defines which css class is set for the nodes of the tree.	
	this.visualizationHtmlElement = this.getVisualizationHtmlElement();	 // Get HTML element of the visualization container;
	this.treeHtmlElement;					// Will hold the html element of the tree only (not the legenda or controls).
	this.treeHtmlElementHeight;				// The height of the html element of the tree. Parts of the tree that do not fit inside this height are not visible.
	this.treeHtmlElementWidth;				// The width of the html element of the tree. Parts of the tree that do not fit inside this height are not visible.
	this.controlsHtmlElement;				// Will hold the html element of the controls.
	this.legendaHtmlElement;				// Will hold the html element of the legenda.
	this.controlPlay = null;				// Will hold the html play button
	this.controlNext = null;				// Will hold the html next button
	this.controlPrevious = null;			// Will hold the html previous button
	this.controlPause = null;				// Will hold the html pauze button
	this.controlReset = null;				// Will hold the html reset button
}

/* ########################################## PUBLIC METHODS ##########################################  */

/*
	This method sets whether controls should be shown for the visualization. Argument 'value' must be either true of false
*/
TreeVisualization.prototype.showControls = function( value ){
	this.addControls = value;
}

/*
	This method sets whether a legenda should be shown as part of the visualization. Argument 'value' must be either true of false
*/
TreeVisualization.prototype.showLegenda = function( value ){
	this.addLegenda = value;
}

/*
	This method sets the height of the tree html element in pixels.
*/
TreeVisualization.prototype.setTreeViewHeight = function( height ){
	this.treeHtmlElementHeight = height;
}

/*
	This method sets the width of the tree html element in pixels. 
*/
TreeVisualization.prototype.setTreeViewWidth = function( width ){
	this.treeHtmlElementWidth = width;
}

/*
	This method sets which css class is applied to the nodes of the tree. Argument 'className' should be the name of a css class.
*/
TreeVisualization.prototype.setNodeClassName = function( className ){
	this.nodeClassName = className;
}


/*
	This methods set the size (width and height) of the tree nodes in pixels.
*/
TreeVisualization.prototype.setNodeSize = function( width, height ){
	this.nodeWidth = width;
	this.nodeHeight = height;
}

/*
	This method sets whether it should be possible for users to drag the tree or not. Can be usefull for large trees that can not be shown entirely in the html view.
*/
TreeVisualization.prototype.setAllowDragging = function( allowDragging ){
	this.allowDragging = allowDragging;
}

/*
	This sets whether the labels of the tree should be shown or not.
*/
TreeVisualization.prototype.setAddLabels = function( addLabels ){
	this.addLabels = addLabels;
}

/*
	Sets the offset for the root note to the middle of the tree html element. The offsets determine how far from the middle of the html view the root node is placed (in pixels).
*/
TreeVisualization.prototype.setOffsets = function( x, y ){
	this.offsetX = x;
	this.offsetY = y;
}

/*
	This method sets the tree that is used for the visualization. The argument must be a TreeNode object.
*/
TreeVisualization.prototype.setTree = function( tree ){
	this.tree = tree;
}

/*
	This method sets whether the tree is a multiplayer tree or not. (ie. the levels of the tree correspond to different 'players'. (This changes the layout of the tree).
*/
TreeVisualization.prototype.setMultiplayer = function( isMultiplayerTree ){
	this.isMultiplayerTree = isMultiplayerTree;
}

/*
	This method sets the play speed of the visualization. If the visualization is automatically played. This determines how fast it will perform the steps in the algorithm.
*/
TreeVisualization.prototype.setPlaySpeed = function( speed ){
	this.playSpeed = speed; // in milliseconds
}

/*
	This sets the goal of the algorithm. This is the data that the algorithm will be looking for. Nodes containing this data are marked as goal nodes.
*/
TreeVisualization.prototype.setGoal = function( goalData ){
	this.goal = goalData;
}

/*
	This method sets whether random heuristics should be generated for the tree. Argument 'value' must either be true or false.
*/
TreeVisualization.prototype.useRandomHeuristics = function( value ){
	this.randomHeuristics = value;
}

/*
	This method sets whether the heuristic of a node should be used as its label. Argument 'value' must either be true or false. By default this is set to true.
*/
TreeVisualization.prototype.useHeuristicAsLabel = function( value ){
	this.heuristicAsLabel = value;
}

/*
	This sets a random goal but only works if the nodes in the tree have subsequent values (so 1,2,3,4,5 ). It doesn't break if this is not the case, but then there might be no goal node in the tree. 
*/ 
TreeVisualization.prototype.setRandomGoal = function(){
	var randomNodeValue = Math.floor( Math.random()*this.tree.getTreeSize() );
	this.goal = randomNodeValue;
}

/*
	This method sets the beam size. This is only used for the Beam Search algorithm. The size defines the number of nodes in the beam.
*/
TreeVisualization.prototype.setBeamSize = function( size ){
	this.beamSize = size;
}

/*
	This method sets the search type that will determine which search algorithm is used in the visualization. Argument 'searchType' must be one of the following values: bfs, dfs, id, beam, hc, best, A, Astar, minimax.
*/
TreeVisualization.prototype.selectAlgorithm = function( searchType ){
	switch( searchType ){
		case 'bfs':		this.searchType = 'bfs';		break; 
		case 'dfs':		this.searchType = 'dfs';		break; 
		case 'id':		this.searchType = 'id';			break; 
		case 'beam':	this.searchType = 'beam';		break; 
		case 'hc':		this.searchType = 'hc';			break; 
		case 'best':	this.searchType = 'best';		break; 
		case 'A':		this.searchType = 'A';			break; 
		case 'Astar':	this.searchType = 'Astar';		break; 
		case 'minimax': this.searchType = 'minimax';	break;
		default: throw new Error('invalid search type supplied for method TreeVisualization.prototype.setSearchType(). Allowed values:  bfs, dfs, id, beam, hc, best, A, Astar, minimax.');
	}	
}

/*
	This method starts the visualization with all the settings that have been specified. Chaning settings after this method has been called does not have any effect.
*/
TreeVisualization.prototype.startVisualization = function(){
	if( !this.searchType ){
		throw new Error('A visualization cannot be started without first specifying a search type.');
	} else if( !this.tree ){
		throw new Error('A visualization cannot be started without specifying the tree structure that is to be displayed.');
	}

	// Create legenda or not depending on the settings.
	if( this.addLegenda === true ){
		this.createLegenda();
	}

	// Set random heuristics for the tree entire tree.
	if( this.randomHeuristics === true ){
		this.setRandomHeuristics( this.tree );
	}
	
	this.createTreeUi(); // display the tree in the html view.

	// Create controls or not depending on the settings.
	if( this.addControls === true ){
		this.createControls();
	}

	this.initSearch();
	this.updateControlAvailability('manual');
	this.refreshMarks(); // Refresh the user interface of the tree.
}


// ############################################################################ Private methods ########################################################################

/*
	This methods is used to determine what the default setting for the allowDragging property should be. This property defines whether a tree should be able to be dragged or not.
	At low resolution this should be set to true because parts of the tree might not be visible with low resolutions so dragging the tree around is a must then.
*/
TreeVisualization.prototype.detectAllowDragging = function(){
	if( window.screen.width <= 1177 ){
		return true;
	} else{
		return false;
	}
}


/*
	This method creates the actual spacetree User Interface. It draws the tree on the screen using the spacetree library.
*/
TreeVisualization.prototype.createTreeUi = function(){
	this.createTreeHtmlElement();
	this.spacetreeUi = new SpacetreeUi( 'tree_' + this.visualizationId, this.tree );
	this.spacetreeUi.setAllowDragging( this.allowDragging );
	this.spacetreeUi.setAddLabels( this.addLabels );
	this.spacetreeUi.setOffsets( this.offsetX, this.offsetY );
	this.spacetreeUi.setMultiplayerTree( this.isMultiplayerTree );
	this.spacetreeUi.setNodeSize( this.nodeWidth, this.nodeHeight );
	this.spacetreeUi.setNodeClassName( this.nodeClassName );
	this.spacetreeUi.createUi();
}

/*
	This method initializes the search algorithm that is to be performed on the tree. Which algorithm will be initialized is specified by the this.searchType property.
	By initializing the search, the searchAlgorithm object is created and knows where to start and what to look for.
*/
TreeVisualization.prototype.initSearch = function(){
	if( !this.searchType ){
		throw new Error('No searchtype is specified. This must be done before starting the visualization.');
	}
	var startNode = new SearchNode( this.tree.getValue(), this.tree, 0, this.tree.getHeuristic() );
	var goalNode = new SearchNode( this.goal );

	switch( this.searchType ){
		case 'bfs':			this.search = new BreadthFirstSearch( startNode, goalNode, bind( this, this.getTreeChildren ) );
							break;
		case 'dfs':			this.search = new DepthFirstSearch( startNode, goalNode, bind( this, this.getTreeChildren ) ); 
							break;
		case 'id':			this.search = new IterativeDeepening( startNode, goalNode, bind( this, this.getTreeChildren ) ); 
							break;
		case 'beam':		if( this.heuristicAsLabel == true ){ this.setHeuristicAsLabel( this.tree ); }
							this.search = new BeamSearch( startNode, goalNode, bind( this, this.getTreeChildren ), 4 );
							this.search.beamSize = this.beamSize;
							break;
		case 'hc':			if( this.heuristicAsLabel == true ){ this.setHeuristicAsLabel( this.tree ); }
							this.search = new BeamSearch( startNode, goalNode, bind( this, this.getTreeChildren ), 1 );
							break;
		case 'best':		if( this.heuristicAsLabel == true ){ this.setHeuristicAsLabel( this.tree ); }
							this.search = new BeamSearch( startNode, goalNode, bind( this, this.getTreeChildren ), Number.INFINITY );
							break;
		case 'A':			if( this.heuristicAsLabel == true ){ this.setHeuristicPlusDepthAsLabel( this.tree ); }
							this.search = new A( startNode, goalNode, bind( this, this.getTreeChildren ) );
							break;
		case 'Astar':		if( this.heuristicAsLabel == true ){ this.setHeuristicPlusDepthAsLabel( this.tree ); }
							this.search = new A( startNode, goalNode, bind( this, this.getTreeChildren ) );
							break;
		case 'minimax':		if( this.heuristicAsLabel == true ){ this.setHeuristicAsLabel( this.tree ); }
							this.search = new Minimax( startNode, bind( this, this.getTreeChildren ) );
							break;
		default:			throw new Error('No valid searchtype is specified, so no search can be initialized.');
	}
}

/*
	This method performs one step of the search algorithm and reflects the changes in the visualization.
*/
TreeVisualization.prototype.stepSearch = function(){
	this.search.step();

	// Clear the interval because the algorithm is done.
	if( this.search.isDone() ){
		clearInterval( this.playIntervalHandler );
		this.updateControlAvailability('done');
	}

	this.refreshMarks();
}

/*
	This method undo one step of the algorithm and reflects changes in the visualization.
*/
TreeVisualization.prototype.undoStepSearch = function(  ){
	this.search.undoStep();
	this.refreshMarks();
}

/*
	This method refreshes the tree, by replotting it. This is used ro reflect any changes that have been performed on the tree.
*/
TreeVisualization.prototype.refreshUi = function(){
	this.spacetreeUi.replotTree();
}


/*
	This method refreshes the marks on the nodes of the tree. This is needed to update the state of all the nodes in the tree. 
	For example when a node has changed from being an open node to a visited node, this change must be processed so that it is be shown on the User Interface.
*/
TreeVisualization.prototype.refreshMarks = function(){
	this.removeAllMarks( this.tree );
	switch( this.searchType ){
		case 'bfs':		this.markClosedNodes();	
						this.markOpenNodes();
						break;
		case 'dfs':		this.markClosedNodes();	
						this.markOpenNodes();
						break;
		case 'id':		this.markClosedNodes();	
						this.markOpenNodes();
						break;
		case 'hc':		this.markClosedNodes();
						break;
		case 'best':	this.markClosedNodes();
						this.markOpenNodes();
						break;
		case 'beam':	this.markClosedNodes();
						this.markDiscardedNodes();
						this.markOpenNodes();
						break;
		case 'A':		this.markClosedNodes();
						this.markOpenNodes();
						break;
		case 'Astar':	this.markClosedNodes();
						this.markOpenNodes();
						break;
		case 'minimax': if( this.heuristicAsLabel == true ){ this.setHeuristicAsLabel( this.tree ); }
						break;
	}

	// mark the goal nodes.
	this.markGoalNodes( this.tree );
	
	if( this.search.goalReached ){
		this.markFinish(); // mark the goal node that is found by the algorithm
	}
	
	this.refreshUi();
}


/*
	This method marks all open nodes as 'open', so that it can be displayed as such by the tree ui.
*/
TreeVisualization.prototype.markOpenNodes = function(){
	var openList = this.search.openList;
	for( var i=0; i<openList.getSize(); i++ ){
		var node = openList.getElementAt( i );
		var treeNode = node.getNode();
		treeNode.setNodeClass( 'open' );
	}
}


/*
	This method marks all closed/visited nodes as 'closed', so that it can be displayed as such by the tree ui.
*/
TreeVisualization.prototype.markClosedNodes = function(){
	var closedList = this.search.closedList;
	for( var i=0; i<closedList.getSize(); i++ ){
		var node = closedList.getElementAt( i );
		var treeNode = node.getNode();
		treeNode.setNodeClass( 'closed' );
	}
}

/*
	This method marks all discarded nodes as 'discarded', so that it can be displayed as such by the tree ui. This is only used by the beam search algorithm
*/
TreeVisualization.prototype.markDiscardedNodes = function(){
	var discardedList = this.search.discardedList;
	for( var i=0; i<discardedList.getSize(); i++ ){
		var node = discardedList.getElementAt( i );
		var treeNode = node.getNode();
		treeNode.setNodeClass( 'discarded' );
	}
}

/*
	This method marks all goal nodes as a goal node, so that it can be displayed as such by the tree ui.
*/
TreeVisualization.prototype.markGoalNodes = function( node ){
	if( node.getValue() == this.goal ){
		node.setAsGoal();
	}

	for( var i=0; i<node.getNumberOfChildren(); i++ ){
		this.markGoalNodes( node.getChild( i ) );
	}
	return;
}

/*
	This method marks a goal node that has been found by the algorithm.
*/
TreeVisualization.prototype.markFinish = function(){
	// can be added if needed.
}

/*
	This method removes all marks from a tree. So that all nodes are just regular nodes unmarked nodes.
*/
TreeVisualization.prototype.removeAllMarks = function( root ){
	root.deleteNodeClass();

	for( var i=0; i<root.getNumberOfChildren(); i++ ){
		this.removeAllMarks( root.getChild( i ) );
	}

	return;
}

/*
	This sets the nodeClass value for a TreeNode object. The SpacetreeUi uses this value to determine how to display the node.
*/
TreeVisualization.prototype.setNodeClass = function( node, nodeClass ){
	node.setNodeClass( nodeClass );
}



/*
	This methods adds a random heuristic to all nodes of the tree. This just calls the recursive function so that it doesn't need an argument value.  This just calls the recursive function so that it doesn't need an argument value.
*/
TreeVisualization.prototype.setRandomHeuristics = function(){
	this.setRandomHeuristicsRecursive( this.tree );
}

/*
	This methods adds a random heuristic to all nodes of the tree. This just calls the recursive function so that it doesn't need an argument value.
*/
TreeVisualization.prototype.setRandomHeuristicsRecursive = function( root ){
	var randomHeuristic = Math.floor( Math.random()*20 );
	root.setHeuristic( randomHeuristic );

	for( var i=0; i<root.getNumberOfChildren(); i++ ){
		this.setRandomHeuristicsRecursive( root.getChild( i ) );
	}
	return;
}

/*
	This method sets the heuristic of each node as the label field of that node, so that the label is just the heuristic.  This just calls the recursive function so that it doesn't need an argument value.
*/
TreeVisualization.prototype.setHeuristicAsLabel = function(){
	this.setHeuristicAsLabelRecursive( this.tree );
}

/*
	This method sets the heuristic of each node as the label field of that node, so that the label is just the heuristic.
*/
TreeVisualization.prototype.setHeuristicAsLabelRecursive = function( root ){
	var label = root.data.heuristic;
	if( label != null ){
		root.setLabel( label );
	} else{
		root.setLabel( '' );
	}

	for( var i=0; i<root.getNumberOfChildren(); i++ ){
		this.setHeuristicAsLabelRecursive( root.getChild( i ) );
	}
	return;
}

/*
	This method sets the heuristic+depth of each node as the label field of that node, so that the label is just the heuristic+depth=new heuristic.  This just calls the recursive function so that it doesn't need an argument value.
*/
TreeVisualization.prototype.setHeuristicPlusDepthAsLabel = function(){
	this.setHeuristicPlusDepthAsLabelRecursive( this.tree );
}

/*
	This method sets the heuristic+depth of each node as the label field of that node, so that the label is just the heuristic+depth=new heuristic.
*/
TreeVisualization.prototype.setHeuristicPlusDepthAsLabelRecursive = function( node ){
	var depth = node.getDepth();
	var heuristic = node.data.heuristic;
	var label = depth + '+' + heuristic  + '=' + (depth+heuristic);

	if( label != null ){
		node.setLabel( label );
	}

	for( var i=0; i<node.getNumberOfChildren(); i++ ){
		this.setHeuristicPlusDepthAsLabelRecursive( node.getChild( i ) );
	}
	return;
}


/*
	This is the callback function for the search algorithm to find out the children of a given node.
	The found children are wrapped in a searchnode and returned to the search algorithm.
*/
TreeVisualization.prototype.getTreeChildren = function( currentNode ){
	var currentDepth = currentNode.getDepth();
	var childTreeNodes = currentNode.getNode().getChildren();
	var childrenSearchNodes = new Vector();


	for( var i=0; i<childTreeNodes.getSize(); i++ ){
		var childTreeNode = childTreeNodes.getElementAt( i );
		var heuristic =  childTreeNode.getHeuristic();
		var searchNode = new SearchNode( childTreeNode.getData().value, childTreeNode, currentDepth+1, heuristic );
		childrenSearchNodes.addElement( searchNode );
	}

	return childrenSearchNodes;
}

/* 
	This method generates a random tree which can be used in the visualization.	It takes three arguments:
	'numberOfLevels': this determines how deep the tree will be.
	'branchingFactor': this determines how many childs each node will have.
	'addLabel': this determines whether a label should be added to the tree.
*/


TreeVisualization.prototype.generateTree = function( numberOfLevels, branchingFactor, addLabel ){
	if( typeof numberOfLevels == 'undefined' || numberOfLevels == null || typeof branchingFactor == 'undefined' || branchingFactor == null || typeof addLabel == 'undefined' || addLabel == null ){
		throw new Error('Missing argument value for TreeVisualization.createTree() method.');
	}

	var nodesToAddChildrenTo = new Vector();

	// Create root node
	var label = (addLabel == true) ?  '0' : null;
	var data = {'value': 0};
	var nodeId = this.visualizationId+'_0';
	var root = new TreeNode( nodeId, data, label);

	// add it to the list of nodes where children needs to be added to.
	nodesToAddChildrenTo.addElement( root );
	
	var counter=1;
	while( nodesToAddChildrenTo.getSize() > 0 ){
		var parentNode = nodesToAddChildrenTo.getFirstElement();
		nodesToAddChildrenTo.removeFirstElement();
		var currentDepth = parentNode.getDepth();
		if( currentDepth < numberOfLevels-1 ){
			for( var i=0; i<branchingFactor; i++ ){
				// create child node.
				label = (addLabel == true) ?  counter : null;
				data = {'value': counter};
				nodeId = this.visualizationId+'_' +counter;
				var childNode = new TreeNode( nodeId, data, label);
				parentNode.addChild( childNode );

				// add it  it to the list of nodes where children needs to be added to.
				nodesToAddChildrenTo.addElement( childNode );
				counter++;
			}		
		}		
	}
	this.tree = root; // set it as the tree of the visualization.
}

/*
	This method creates the legenda of the visualization, which shows how nodes are displayed for all relevant nodes of the current search type. It also creates the html element in which the legenda is shown.
*/
TreeVisualization.prototype.createLegenda = function(){
	this.createLegendaHtmlElement();

	switch( this.searchType ){
		case 'beam':
			this.createLegendaItem( 'defaultNode', '       ', '   = onbezochte knoop  ' );
			this.createLegendaItem( 'closedNode', '       ', '   = bezochte knoop  ' );
			this.createLegendaItem( 'openNode', '       ', '   = Open knoop  ' );
			this.createLegendaItem( 'goalNode', '       ', '   = Doel knoop  ' );
			this.createLegendaItem( 'discardedNode', ' vergeten ', '   = knoop paste niet in beam  ' );
			break;
		case 'hc':
			this.createLegendaItem( 'defaultNode', '       ', '   = onbezochte knoop  ' );
			this.createLegendaItem( 'closedNode', '       ', '   = bezochte knoop  ' );
			this.createLegendaItem( 'goalNode', '       ', '   = Doel knoop  ' );
			break; 
		case 'minimax':			
			this.createLegendaItem( 'minNode', '       ', '   = Min speler  ' );
			this.createLegendaItem( 'maxNode', '       ', '   = Max speler  ' );
			break;
		default:	
			this.createLegendaItem( 'defaultNode', '       ', '   = onbezochte knoop  ' );
			this.createLegendaItem( 'closedNode', '       ', '   = bezochte knoop  ' );
			this.createLegendaItem( 'openNode', '       ', '   = Open knoop  ' );
			this.createLegendaItem( 'goalNode', '       ', '   = Doel knoop  ' );
			break;
	}	
}

/*
	This method creates on item of the legenda. For example the item for an opennode, that shows how an open node is displayed.
*/
TreeVisualization.prototype.createLegendaItem = function( className, iconText, labelText ){
	var legendaIcon = document.createElement('span');
	legendaIcon.style.whiteSpace = 'pre';
	var iconTextNode = document.createTextNode( iconText );
	legendaIcon.className = className;
	legendaIcon.appendChild( iconTextNode );

	var legendaLabel = document.createElement('span');
	legendaLabel.style.whiteSpace = 'pre';
	var labelTextNode = document.createTextNode( labelText );
	legendaLabel.appendChild( labelTextNode );
	
	var legendaItem = document.createElement('span');
	legendaItem.appendChild( legendaIcon );
	legendaItem.appendChild( legendaLabel );
	
	this.legendaHtmlElement.appendChild( legendaItem );
}

/* 
	This method checks if a HTML element exists with id 'visualization_<visualizationId>'. If no such html element exists, it is created and appended to the html body element. This html element will contain the whole visualization.
*/
TreeVisualization.prototype.getVisualizationHtmlElement = function(){
	var visualizationHtmlElement = document.getElementById("visualization_" + this.visualizationId );
	if( !visualizationHtmlElement ){
		visualizationHtmlElement = this.createVisualizationHtmlElement();
		var htmlBodyElement = document.getElementsByTagName("body")[0];
		htmlBodyElement.appendChild( visualizationHtmlElement );
	}
	
	return visualizationHtmlElement;
}

/*
	This method creates a HTML which will contain the visualization. It sets the id and name of the element to: 'visualization_<visualizationId>' where <visualizationId> is the visualizationId property of this object.
	This will only succeed if no other element exists with same id.	The newly created element is added to the html body element.
*/
TreeVisualization.prototype.createVisualizationHtmlElement = function(){
	var visualizationHtmlElement = document.getElementById("visualization_" + this.visualizationId );
	if( visualizationHtmlElement ){
		throw new Error('There allready is a html element with the id "visualization_' + this.visualizationId + '", TreeVisualization.prototype.createVisualizationHtmlElement() cannot create an element with the same id.');
	}
	
	visualizationHtmlElement = document.createElement('div');
	visualizationHtmlElement.setAttribute('id','visualization_' + this.visualizationId );
	visualizationHtmlElement.setAttribute('name','visualization_' + this.visualizationId );
	visualizationHtmlElement.setAttribute('class','treeVisualizationContainer' );

	return visualizationHtmlElement;
}

/*
	This method creates the html element that will hold the tree. It sets the id and name of the element to: 'tree_<visualizationId>' where <visualizationId> is the visualizationId property of this object.
	This will only succeed if no other element exists with same id.	The newly created element is added to the html element of the visualization.
*/
TreeVisualization.prototype.createTreeHtmlElement = function(){
	// Check if there allready is an element with the same id. In that case the element can not be created.
	var treeHtmlElement = document.getElementById("tree_" + this.visualizationId );
	if( treeHtmlElement ){
		throw new Error('There allready is a html element with the id "tree_' + this.visualizationId + '", TreeVisualization.prototype.createTreeHtmlElement() cannot create an element with the same id.');
	}
	
	// Create the html element for the tree.
	treeHtmlElement = document.createElement('div');
	treeHtmlElement.setAttribute('id', 'tree_' + this.visualizationId );
	treeHtmlElement.setAttribute('name', 'tree_' + this.visualizationId );
	treeHtmlElement.setAttribute('class', 'treeVisualization' );

	// Set the width and height of the tree html element if they are provided.
	if( this.treeHtmlElementHeight ){
		treeHtmlElement.style.height = this.treeHtmlElementHeight + 'px';
	} 
	if( this.treeHtmlElementWidth ){
		treeHtmlElement.style.height = this.treeHtmlElementWidth + 'px';
	}

	this.visualizationHtmlElement.appendChild( treeHtmlElement );
	this.treeHtmlElement = treeHtmlElement;
}

/*
	This method creates the html element that will hold the controls of the visualization. It sets the id and name of the element to: 'controls_<visualizationId>' where <visualizationId> is the visualizationId property of this object.
	This will only succeed if no other element exists with same id. The newly created element is added to the html element of the visualization.
*/
TreeVisualization.prototype.createControlsHtmlElement = function(){
	// Check if there allready is an element with the same id. In that case the element can not be created.
	var controlsHtmlElement = document.getElementById("controls_" + this.visualizationId );
	if( controlsHtmlElement ){
		throw new Error('There allready is a html element with the id "controls_' + this.visualizationId + '", TreeVisualization.prototype.createControlsHtmlElement() cannot create an element with the same id.');
	}

	controlsHtmlElement = document.createElement('div');
	controlsHtmlElement.setAttribute('id', 'controls_' + this.visualizationId );
	controlsHtmlElement.setAttribute('name', 'controls_' + this.visualizationId );
	controlsHtmlElement.setAttribute('class', 'controls' );
	this.visualizationHtmlElement.appendChild( controlsHtmlElement );
	
	this.controlsHtmlElement = controlsHtmlElement;
}


/*
	This method creates the html element that will hold the legenda of the visualization. It sets the id and name of the element to: 'legenda_<visualizationId>' where <visualizationId> is the visualizationId property of this object.
	This will only succeed if no other element exists with same id. The newly created element is added to the html element of the visualization.
*/
TreeVisualization.prototype.createLegendaHtmlElement = function(){
	// Check if there allready is an element with the same id. In that case the element can not be created.
	var legendaHtmlElement = document.getElementById("legenda_" + this.visualizationId );
	if( legendaHtmlElement ){
		throw new Error('There allready is a html element with the id "legenda_' + this.visualizationId + '", TreeVisualization.prototype.createLegendaHtmlElement() cannot create an element with the same id.');
	}

	legendaHtmlElement = document.createElement('div');
	legendaHtmlElement.setAttribute('id', 'legenda_' + this.visualizationId );
	legendaHtmlElement.setAttribute('name', 'legenda_' + this.visualizationId );
	legendaHtmlElement.setAttribute('class', 'legenda' );
	this.visualizationHtmlElement.appendChild( legendaHtmlElement );
	
	this.legendaHtmlElement = legendaHtmlElement;
}

/*
	This method will automatically step through the algorithm at a speed that is defined in the this.playSpeed property.
	It sets an interval and stores the handler of it in this.playIntervalHandler, which can be used to cancel it. Also it will enable/disable the appropriate control buttons.
*/
TreeVisualization.prototype.playSearch = function(){
	var bound = bind( this, this.stepSearch );
	this.playIntervalHandler = setInterval( bound, this.playSpeed );	
	if( this.search.isDone() ){
		this.updateControlAvailability('done');
	} else{
		this.updateControlAvailability('auto');
	}
}


/*
	This will stop the interval that is performing search steps automatically. Also it will enable/disable the appropriate control buttons.
*/
TreeVisualization.prototype.pauseSearch = function(){
	clearInterval( this.playIntervalHandler );
	if( this.search.isDone() ){
		this.updateControlAvailability('done');
	} else{
		this.updateControlAvailability('manual');
	}
}

/*
	This will reset the search and reflect the changes to the screen. Also it will enable/disable the appropriate control buttons.
*/
TreeVisualization.prototype.resetSearch = function(){
	clearInterval( this.playIntervalHandler );
	this.removeAllMarks( this.tree );
	this.search.reset();
	this.refreshMarks();
	if( this.search.isDone() ){
		this.updateControlAvailability('done');
	} else{
		this.updateControlAvailability('manual');
	}
}

/*
	This handles a click on the previous button. Also it will enable/disable the appropriate control buttons.
*/
TreeVisualization.prototype.previousSearch = function(){
	this.undoStepSearch();
	if( this.search.isDone() ){
		this.updateControlAvailability('done');
	} else{
		this.updateControlAvailability('manual');
	}
}


/*
	This handles a click on the next button. Also it will enable/disable the appropriate control buttons.
*/
TreeVisualization.prototype.nextSearch = function(){
	this.stepSearch();
	if( this.search.isDone() ){
		this.updateControlAvailability('done');
	} else{
		this.updateControlAvailability('manual');
	}
}


/*
	This will enable/disable the appropriate controls considering the supplied argument 'state'. Valid 'state' values are: manual, auto.
	The 'auto' state means that the visualization is being played automatically. The 'manual' state means it is not played automatically.
*/
TreeVisualization.prototype.updateControlAvailability = function( newState ){
	if( typeof newState == 'undefined' ){
		throw new Error('control availability can not be determined without supplying a state.');
	}
	
	if( newState == 'done' ){
		this.controlPause.disabled = true;
		this.controlReset.disabled = false;
		this.controlPlay.disabled = true;
		this.controlNext.disabled = true;
		this.controlPrevious.disabled = false;
		return;
	} else if( newState == 'auto' ){
		this.controlPause.disabled = false;
		this.controlReset.disabled = false;
		this.controlPlay.disabled = true;
		this.controlNext.disabled = true;
		this.controlPrevious.disabled = true;
		return;
	} else if( newState == 'manual' ){
		this.controlPause.disabled = true;
		this.controlReset.disabled = false;
		this.controlPlay.disabled = false;
		this.controlNext.disabled = false;
		this.controlPrevious.disabled = ( this.search.stepNumber == 0 ) ? true : false;
		return;
	}

}

/*
	This function creates the controls that can be used to control the visualization
*/
TreeVisualization.prototype.createControls = function(){
	this.createControlsHtmlElement();

	// create play button
	this.controlPlay = document.createElement("input");
	this.controlPlay.setAttribute("id",this.visualizationId+'_play');
	this.controlPlay.setAttribute("name",this.visualizationId+'_play');
	this.controlPlay.setAttribute("type","button");
	this.controlPlay.setAttribute("value","Play");
	
	// create pause button
	this.controlPause = document.createElement("input");
	this.controlPause.setAttribute("id",this.visualizationId+'_pause');
	this.controlPause.setAttribute("name",this.visualizationId+'_pause');
	this.controlPause.setAttribute("type","button");
	this.controlPause.setAttribute("value","Pauze");

	// create next button
	this.controlNext = document.createElement("input");
	this.controlNext.setAttribute("id",this.visualizationId+'_next');
	this.controlNext.setAttribute("name",this.visualizationId+'_next');
	this.controlNext.setAttribute("type","button");
	this.controlNext.setAttribute("value","Volgende");

	// Create back button
	this.controlPrevious = document.createElement("input");
	this.controlPrevious.setAttribute("id",this.visualizationId+'_previous');
	this.controlPrevious.setAttribute("name",this.visualizationId+'_previous');
	this.controlPrevious.setAttribute("type","button");
	this.controlPrevious.setAttribute("value","Vorige");

	// Create reset button
	this.controlReset = document.createElement("input");
	this.controlReset.setAttribute("id",this.visualizationId+'_reset');
	this.controlReset.setAttribute("name",this.visualizationId+'_reset');
	this.controlReset.setAttribute("type","button");
	this.controlReset.setAttribute("value","Reset");

	// append the controls to their container and the container to the parent element.
	this.controlsHtmlElement.appendChild(this.controlPrevious);
	this.controlsHtmlElement.appendChild(this.controlNext);
	this.controlsHtmlElement.appendChild(this.controlPause);
	this.controlsHtmlElement.appendChild(this.controlPlay);
	this.controlsHtmlElement.appendChild(this.controlReset);
	
	// Set event handlers for the controls.
	this.controlPrevious.onclick = bind(this, this.previousSearch );
	this.controlNext.onclick = bind(this, this.nextSearch );
	this.controlPlay.onclick = bind(this, this.playSearch );
	this.controlPause.onclick = bind(this, this.pauseSearch );
	this.controlReset.onclick = bind(this, this.resetSearch );
}