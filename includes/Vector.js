function Vector() {
	this.initialCapacity = 10; // the ammount of elements the vector can initially hold.
	this.vector = new Array( this.initialCapacity ); // the actual array which will be holding the vector data.
	this.size = 0;  // the number of elements stored in the vector.
}


// returns the number of elements the vector can hold
Vector.prototype.getCapacity = function(){
	return this.vector.size; // size of the underlying array.
}

// returns the number of elements currently stored in the vector.
Vector.prototype.getSize = function() {
	return this.size;
}

// Check if the vector is empty (true if it has 0 elements, false otherwise).
Vector.prototype.isEmpty = function() {
	return this.getSize() == 0;
}

// Returns the first element of the vector
Vector.prototype.getFirstElement = function(){
	if( this.getSize() == 0 ){
		throw new Error("Can't get the first element of an empty vector.");
	} else{
		return this.vector[0];
	}
}


// Returns the last element of the vector.
Vector.prototype.getLastElement = function(){
	if( this.getSize() == 0 ){
		throw new Error("Can't get the last element of an empty vector.");
	} else{
		return this.vector[ this.getSize()-1 ];
	}
}


// Returns an element at the specified index
Vector.prototype.getElementAt = function( i ){
	if( i > this.getSize()-1 || i < 0 ){
		throw new Error("getElementAt() could not get the element from the given index, because the index does not exist.");
	} else{
		return this.vector[i];
	}
}

// Add an element at the end of the vector
Vector.prototype.addElement = function( obj ){
	// Check if the vector has reached its capacity, if it has, resize the vector.
	if( this.getSize() == this.getCapacity() ){
		this.resize();
	}
	
	this.vector[ this.size ] = obj;
	this.size += 1;
	return this;
}

// Add an element at the front of the vector
Vector.prototype.addFirst = function( obj ){
	// Check if the vector has reached its capacity, if it has, resize the vector.
	if( this.getSize() == this.getCapacity() ){
		this.resize();
	}
	
	// shift all values to make room for the new value.
	for( var i=this.getSize(); i > 0; i-- ){
		this.vector[i] = this.vector[i-1];
	}

	this.vector[ 0 ] = obj;
	this.size += 1;
	return this;
}


Vector.prototype.insertElementAt = function( element, index ){
	// Check if the index is valid.
	if( index > this.getSize() || index < 0 ){
		throw new Error("Can't insert the element at the given index, the index is invalid.");
	}

	// If the capacity of the vector has been reached, increase the capacity.
	if( this.getSize() == this.getCapacity() ){
		this.resize();
	}
	
	// shift all values to make room for the new value.
	for( var i=this.getSize(); i > index; i-- ){
		this.vector[i] = this.vector[i-1];
	}
	
	// add the new value.
	this.vector[ index ] = element;
	this.size += 1;
	return this;
}


// removes an element at the given index and shifts the other elements to fill up the emptied space.
Vector.prototype.removeElementAt = function( index ){
	// Check if the index is valid.
	if( index >= this.getSize() || index < 0 ){
		throw new Error("Can't remove element at the given index, the index is invalid.");
	}

	for( var i=index; i<this.getSize()-1; i++ ){
		this.vector[ i ] = this.vector[ i+1 ];
	}

	this.vector[ this.getSize()-1 ] = null;		
	this.size -= 1;

	return this;
}


// Removes the last element.
Vector.prototype.removeLastElement = function(){
	return this.removeElementAt( this.getSize()-1 );
}


// removes the first element.
Vector.prototype.removeFirstElement = function(){
	return this.removeElementAt( 0 );
}


// Removes all elements from the Vector
Vector.prototype.removeAllElements = function(){
	this.size = 0;
	
	for (var i=0; i<this.getCapacity(); i++) {
		this.vector[i] = null;
	}

	this.trimToSize();
	return this;
}


// Remove the given element from the vector.
Vector.prototype.removeElement = function( obj ){
	var index =  this.indexOf( obj );
	if( index == -1 ){
		throw new Error("Trying to remove an object from the vector which does not exist.");
	} else{
		return this.removeElementAt( index );
	}
}

// Returns the index of a searched element
Vector.prototype.indexOf = function( obj ){
/*
	for( var i=0; i<this.getSize(); i++ ){
		var currentObj = this.vector[i];
		if( typeof currentObj == 'number' && typeof obj == 'number' ){
			return (currentObj == obj) ? i : -1;
		} else if( typeof currentObj == 'string' && typeof obj == 'string' ){
			return (currentObj == obj) ? i : -1;;
		} else if( typeof this.node == 'boolean' && typeof obj == 'boolean' ){
			return (currentObj == obj) ? i : -1;;
		} else if( typeof currentObj == 'object' && typeof obj == 'object' ){
			if( currentObj == null && obj == null ){
				return i;
			} else if( currentObj == null || obj == null ){
				return -1;
			} else{
				if( currentObj.hasOwnProperty( 'equals' ) ){
					return (currentObj.equals( obj )) ? i : -1;;
				}
			}
		} else{
			return -1;
		}
	}
*/

	for( var i=0; i < this.getSize(); i++ ) {
		if( this.vector[i] === obj ) {
			return i;
		}
	}
	return -1;
}

// Returns true if the element is in the Vector, otherwise false
Vector.prototype.contains = function( obj ) {
	if( this.indexOf( obj ) != -1 ){
		return true;
	} else{
		return false;
	}
}

// Increase the size of the Vector
Vector.prototype.resize = function() {
	var newVector = new Array( this.getCapacity() * 2 ); // double's the capacity.
	
	for( var i=0; i < this.getCapacity(); i++) {
		newVector[i] = this.getElementAt( i );
	}
	
	this.vector = newVector;
}


// Trim the vector down to it's size
Vector.prototype.trimToSize = function() {
	var newVector = new Array( this.getSize() );
	
	for ( var i = 0; i < this.getSize(); i++ ){
		newVector[i] = this.getElementAt( i );
	}
	
	this.vector = newVector;
} 


// Overwrite the element with another element at the specific index.
Vector.prototype.overwriteElementAt = function( obj, index ){
	this.vector[index] = obj;
}


// returns a copy of this object.
Vector.prototype.clone = function(){
	var clone = new Vector( this.getSize() );
	
	for( var i=0; i < this.getSize(); i++ ){
		clone.addElement( this.getElementAt( i ) );
	}
	
	return clone;
}