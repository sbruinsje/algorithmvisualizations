/*
	This document is a collection of functions that generate trees which are used as examples.
	Most of the code is a complete mess. I wouldn't re-use any of this messy code... :-)
	If you want to add an example, just create a function that returns a tree node object that contains the tree that you want to use as example.
*/

// Generates a tree that will serve as an example to show that bfs always finds the optimal solution.
// The tree has 2 solutions but breadth first search will find the nearest one.
function bfsExample1(treeId){
	
	var numberOfLevels = 5;			// the number of levels the tree has.
	var branchingFactor = 2;		// the maximum number of branches of the tree.
	var nodesToAddChildrenTo = new Vector();
	var root = new TreeNode( treeId+'_0', {'value': 0}, '0');
	nodesToAddChildrenTo.addElement( root );
	var counter=1;
	while( nodesToAddChildrenTo.getSize() > 0 ){
		var parentNode = nodesToAddChildrenTo.getFirstElement();
		nodesToAddChildrenTo.removeFirstElement();
		var currentDepth = parentNode.getDepth();
		if( currentDepth < numberOfLevels-1 ){
			for( var i=0; i<branchingFactor; i++ ){
				var data;
				var label = counter;
				if( counter == 17 || counter == 10 ){ // set the value of nodes 17 and 10 to 1. This is the node that will be searched for.
					data = {'value': 1};
				} else{
					data = {'value': 0};
				}
				var nodeId = treeId+'_' +counter;
				var childNode = new TreeNode( nodeId, data, label);
				parentNode.addChild( childNode );
				nodesToAddChildrenTo.addElement( childNode );
				counter++;
			}		
		}	
	}
	return root;
}


// This generates a tree that can show why depth first search does not always find a solution
function dfsExample1( treeId ){
	var root = new TreeNode( treeId+'_0', {'value': 0}, '0');
	var child1 = new TreeNode( treeId+'_1', {'value': 0}, '1');
	var child2 = new TreeNode( treeId+'_2', {'value': 0}, '2');
	var child3 = new TreeNode( treeId+'_100', {'value': 1}, '100');
	root.addChild( child1 );
	root.addChild( child2 );
	child2.addChild(child3)

	for( var i=0; i<5; i++ ){
		//var value = (i==4) ? 1 : 0;
		var value = 0;
		var node = new TreeNode( treeId+'_'+(3+i), {'value': value}, '3'+i);
		var child=child1;
		for( var j=0; j<i; j++ ){
			child = child.getChild(0);
		}
		child.addChild( node );
	}

	return root;
}


// This generates a tree that can show why depth first search does not always find the optimal solution.
function dfsExample2( treeId ){
	var root = new TreeNode( treeId+'_0', {'value': 0}, '0');
	var child1 = new TreeNode( treeId+'_1', {'value': 0}, '1');
	var child2 = new TreeNode( treeId+'_2', {'value': 0}, '2');
	var child3 = new TreeNode( treeId+'_100', {'value': 1}, '100');
	root.addChild( child1 );
	root.addChild( child2 );
	child2.addChild(child3)

	for( var i=0; i<3; i++ ){
		var value = (i==2) ? 1 : 0;
		var node = new TreeNode( treeId+'_'+(3+i), {'value': value}, '3'+i);
		var child=child1;
		for( var j=0; j<i; j++ ){
			child = child.getChild(0);
		}
		child.addChild( node );
	}

	return root;
}

function idExample1( treeId ){
	var root = new TreeNode( treeId+'_0', {'value': 0}, '0');
	var child1 = new TreeNode( treeId+'_1', {'value': 0}, '1');
	var child2 = new TreeNode( treeId+'_2', {'value': 0}, '2');
	var child3 = new TreeNode( treeId+'_100', {'value': 0}, '100');
	var child4 = new TreeNode( treeId+'_200', {'value': 1}, '200');
	root.addChild( child1 );
	root.addChild( child2 );
	child2.addChild(child3);
	child3.addChild(child4);

	for( var i=0; i<5; i++ ){
		var value = (i==4) ? 1 : 0;
		var node = new TreeNode( treeId+'_'+(3+i), {'value': value}, '3'+i);
		var child=child1;
		for( var j=0; j<i; j++ ){
			child = child.getChild(0);
		}
		child.addChild( node );
	}

	return root;
}



function hcExample1( treeId ){
	var numberOfLevels = 5;			// the number of levels the tree has.
	var branchingFactor = 2;		// the maximum number of branches of the tree.
	var nodesToAddChildrenTo = new Vector();
	var root = new TreeNode( treeId+'_0', {'value': 0}, '0');
	nodesToAddChildrenTo.addElement( root );
	var counter=1;
	while( nodesToAddChildrenTo.getSize() > 0 ){
		var parentNode = nodesToAddChildrenTo.getFirstElement();
		nodesToAddChildrenTo.removeFirstElement();
		var currentDepth = parentNode.getDepth();
		if( currentDepth < numberOfLevels-1 ){
			for( var i=0; i<branchingFactor; i++ ){
				var data= {'value': counter};
				var label = counter;
				var nodeId = treeId+'_' +counter;
				var childNode = new TreeNode( nodeId, data, label);
				parentNode.addChild( childNode );
				nodesToAddChildrenTo.addElement( childNode );
				counter++;
			}		
		}	
	}

	setRandomHeuristic( root, 5 );
	root.data.heuristic = 13;
	var child1 = root.getChild(0);
	child1.data.heuristic = 18;
	var child2 = root.getChild(1);
	child2.data.heuristic = 9;

	var child3 = child2.getChild(0);
	child3.data.heuristic = 4;
	var child4 = child2.getChild(1);
	child4.data.heuristic = 3;
	
	var child5 = child4.getChild(0);
	child5.data.heuristic = 1;	
	var child6 = child4.getChild(1);
	child6.data.heuristic = 4;

	var child7 = child5.getChild(0);
	child7.data.heuristic = 0;	
	var child8 = child5.getChild(1);
	child8.data.heuristic = 10;

	return root;
}



function hcExample2( treeId ){
	var numberOfLevels = 5;			// the number of levels the tree has.
	var branchingFactor = 2;		// the maximum number of branches of the tree.
	var nodesToAddChildrenTo = new Vector();
	var root = new TreeNode( treeId+'_0', {'value': 0}, '0');
	nodesToAddChildrenTo.addElement( root );
	var counter=1;
	while( nodesToAddChildrenTo.getSize() > 0 ){
		var parentNode = nodesToAddChildrenTo.getFirstElement();
		nodesToAddChildrenTo.removeFirstElement();
		var currentDepth = parentNode.getDepth();
		if( currentDepth < numberOfLevels-1 ){
			for( var i=0; i<branchingFactor; i++ ){
				var data= {'value': counter};
				var label = counter;
				var nodeId = treeId+'_' +counter;
				var childNode = new TreeNode( nodeId, data, label);
				parentNode.addChild( childNode );
				nodesToAddChildrenTo.addElement( childNode );
				counter++;
			}		
		}	
	}

	setRandomHeuristic( root, 5 );
	root.data.heuristic = 13;
	var child1 = root.getChild(0);
	child1.data.heuristic = 18;
	var child2 = root.getChild(1);
	child2.data.heuristic = 9;

	return root;
}




function hcExample3( treeId ){
	var root = new TreeNode( treeId+'_0', {'value': 0,'heuristic':6}, '0');
	var child1 = new TreeNode( treeId+'_1', {'value': 0,'heuristic': 1}, '1');
	var child2 = new TreeNode( treeId+'_2', {'value': 0,'heuristic': 2}, '2');
	var child3 = new TreeNode( treeId+'_100', {'value': 0,'heuristic': 1}, '100');
	var child4 = new TreeNode( treeId+'_200', {'value': 1,'heuristic': 0}, '200');
	root.addChild( child1 );
	root.addChild( child2 );
	child2.addChild(child3);
	child3.addChild(child4);

	for( var i=0; i<5; i++ ){
		var value = (i==4) ? 1 : 0;
		var heuristic = (i==4) ? 0 : 5;
		var node = new TreeNode( treeId+'_'+(3+i), {'value': value, 'heuristic': heuristic}, '3'+i);
		var child=child1;
		for( var j=0; j<i; j++ ){
			child = child.getChild(0);
		}
		child.addChild( node );
	}

	return root;
}






function bestExample1( treeId ){
	var root = new TreeNode( treeId+'_0', {'value': 0,'heuristic':6}, '0');
	var child1 = new TreeNode( treeId+'_1', {'value': 0,'heuristic': 10}, '1');
	var child2 = new TreeNode( treeId+'_2', {'value': 0,'heuristic': 11}, '2');
	var child3 = new TreeNode( treeId+'_100', {'value': 1,'heuristic': 1}, '100');
	var child4 = new TreeNode( treeId+'_200', {'value': 0,'heuristic': 0}, '200');
	root.addChild( child1 );
	root.addChild( child2 );
	child2.addChild(child3);
	child3.addChild(child4);

	for( var i=0; i<5; i++ ){
		var value = (i==4) ? 1 : 0;
		var heuristic = (i==4) ? 0 : 10;
		var node = new TreeNode( treeId+'_'+(3+i), {'value': value, 'heuristic': heuristic}, '3'+i);
		var child=child1;
		for( var j=0; j<i; j++ ){
			child = child.getChild(0);
		}
		child.addChild( node );
	}

	return root;

}





function AExample1( treeId ){
	var root = new TreeNode( treeId+'_0', {'value': 0,'heuristic':5}, '0');
	var child1 = new TreeNode( treeId+'_1', {'value': 0,'heuristic': 1}, '1');
	var child2 = new TreeNode( treeId+'_2', {'value': 0,'heuristic': 7}, '2');
	var child3 = new TreeNode( treeId+'_100', {'value': 0,'heuristic': 1}, '100');
	var child4 = new TreeNode( treeId+'_200', {'value': 1,'heuristic': 1}, '200');

	root.addChild( child1 );
	root.addChild( child2 );
	child2.addChild(child3);
	child3.addChild(child4);
	
	for( var i=0; i<8; i++ ){
		var heuristic = 1; //(i==4) ? 0 : 1;
		var value = 0;
		var node = new TreeNode( treeId+'_'+(3+i), {'value': value, 'heuristic': heuristic}, '3'+i);
		var child=child1;
		for( var j=0; j<i; j++ ){
			child = child.getChild(0);
		}
		child.addChild( node );
	}

	return root;
}


function AExample2( treeId ){
	var root = new TreeNode( treeId+'_0', {'value': 0,'heuristic':5}, '0');
	var child1 = new TreeNode( treeId+'_1', {'value': 0,'heuristic': 1}, '1');
	var child2 = new TreeNode( treeId+'_2', {'value': 0,'heuristic': 7}, '2');
	var child3 = new TreeNode( treeId+'_100', {'value': 0,'heuristic': 1}, '100');
	var child4 = new TreeNode( treeId+'_200', {'value': 1,'heuristic': 1}, '200');

	root.addChild( child1 );
	root.addChild( child2 );
	child2.addChild(child3);
	child3.addChild(child4);
	
	for( var i=0; i<5; i++ ){
		var heuristic = 1; //(i==4) ? 0 : 1;
		var value = 0;
		if( i==4 ){
			value = 1;
		}
		var node = new TreeNode( treeId+'_'+(3+i), {'value': value, 'heuristic': heuristic}, '3'+i);
		var child=child1;
		for( var j=0; j<i; j++ ){
			child = child.getChild(0);
		}
		child.addChild( node );
	}

	return root;
}




function AStarExample1( treeId ){
	var numberOfLevels = 5;			// the number of levels the tree has.
	var branchingFactor = 2;		// the maximum number of branches of the tree.
	var nodesToAddChildrenTo = new Vector();
	var root = new TreeNode( treeId+'_0', {'value': 0, 'heuristic': 2}, '0');
	
	// left child of root
	var childNode1 = new TreeNode( treeId+'_1', {'value': 0, 'heuristic': 1}, '0');
	// reight child of root.
	var childNode2 = new TreeNode( treeId+'_2', {'value': 0, 'heuristic': 2}, '0');
	// add them to root.
	root.addChild( childNode1 );
	root.addChild( childNode2 );


	nodesToAddChildrenTo.addElement( childNode1 );
	var counter=3;
	var depthOfGoal = 4;
	while( nodesToAddChildrenTo.getSize() > 0 ){
		var parentNode = nodesToAddChildrenTo.getFirstElement();
		nodesToAddChildrenTo.removeFirstElement();
		var currentDepth = parentNode.getDepth();
		if( currentDepth < numberOfLevels-1 ){
			for( var i=0; i<branchingFactor; i++ ){
				var value = 0;
				if( counter == 10  ){
					value = 1;
				}
				if( currentDepth+1 >= depthOfGoal && value == 0 ){
					heuristic = depthOfGoal + (Math.floor( Math.random()*8));
				} else if( currentDepth+1 == depthOfGoal && value == 1 ){
					heuristic = 0;
				} else if( currentDepth+1 <= depthOfGoal ){
					heuristic = (Math.floor( Math.random()* ((depthOfGoal-(currentDepth+1))+1)));
				}
				var data= {'value': value, 'heuristic':  heuristic };
				var label = counter;
				var nodeId = treeId+'_' +counter;
				var childNode = new TreeNode( nodeId, data, label);
				parentNode.addChild( childNode );
				nodesToAddChildrenTo.addElement( childNode );
				counter++;
			}		
		}	
	}


	var depthOfGoal = 3;
	nodesToAddChildrenTo.addElement( childNode2 );
	while( nodesToAddChildrenTo.getSize() > 0 ){
		var parentNode = nodesToAddChildrenTo.getFirstElement();
		nodesToAddChildrenTo.removeFirstElement();
		var currentDepth = parentNode.getDepth();
		if( currentDepth < numberOfLevels-1 ){
			for( var i=0; i<branchingFactor; i++ ){
				var value = 0;
				if( counter == 21 ){
					value = 1;
				}
				
				if( currentDepth+1 >= depthOfGoal && value == 0 ){
					heuristic = depthOfGoal + (Math.floor( Math.random()*8 ));
				} else if( currentDepth+1 == depthOfGoal && value == 1 ){
					heuristic = 0;
				} else if( currentDepth+1 < depthOfGoal ){
					heuristic = (Math.floor( Math.random()* ((depthOfGoal-(currentDepth+1))+1)));
				}

				var data= {'value': value, 'heuristic':  heuristic };
				var label = counter;
				var nodeId = treeId+'_' +counter;
				var childNode = new TreeNode( nodeId, data, label);
				parentNode.addChild( childNode );
				nodesToAddChildrenTo.addElement( childNode );
				counter++;
			}		
		}	
	}


/*
	setRandomHeuristic( root, 5 );
	root.data.heuristic = 13;
	var child1 = root.getChild(0);
	child1.data.heuristic = 18;
	var child2 = root.getChild(1);
	child2.data.heuristic = 9;
*/

	return root;
}





function beamExample1( treeId ){
	var numberOfLevels = 5;			// the number of levels the tree has.
	var branchingFactor = 2;		// the maximum number of branches of the tree.
	var nodesToAddChildrenTo = new Vector();
	var root = new TreeNode( treeId+'_0', {'value': 0}, '0');
	nodesToAddChildrenTo.addElement( root );
	var counter=1;
	while( nodesToAddChildrenTo.getSize() > 0 ){
		var parentNode = nodesToAddChildrenTo.getFirstElement();
		nodesToAddChildrenTo.removeFirstElement();
		var currentDepth = parentNode.getDepth();
		if( currentDepth < numberOfLevels-1 ){
			for( var i=0; i<branchingFactor; i++ ){
				var value = ( counter == 13) ? 1 : 0;
				var value = ( counter == 19) ? 1 : value;
				var data= {'value': value};
				var label = counter;
				var nodeId = treeId+'_' +counter;
				var childNode = new TreeNode( nodeId, data, label);
				parentNode.addChild( childNode );
				nodesToAddChildrenTo.addElement( childNode );
				counter++;
			}		
		}	
	}

	setRandomHeuristic( root, 5 );
	root.data.heuristic = 13;
	var child1 = root.getChild(0);
	child1.data.heuristic = 18;
	var child2 = root.getChild(1);
	child2.data.heuristic = 20;

	var child3 = child1.getChild(0);
	child3.data.heuristic = 16;
	var child4 = child1.getChild(1);
	child4.data.heuristic = 17;
	
	var child5 = child3.getChild(0);
	child5.data.heuristic = 9;	
	var child6 = child3.getChild(1);
	child6.data.heuristic = 18;

	var child7 = child5.getChild(0);
	child7.data.heuristic = 19;	
	var child8 = child5.getChild(1);
	child8.data.heuristic = 19;

	var child9 = child4.getChild(0);
	child9.data.heuristic = 1;	
	var child10 = child4.getChild(1);
	child10.data.heuristic = 2;

	var child11 = child9.getChild(0);
	child11.data.heuristic = 0;	
	var child12 = child9.getChild(1);
	child12.data.heuristic = 3;


	return root;
}








function miniMaxExample1( treeId ){
	var numberOfLevels = 5;			// the number of levels the tree has.
	var branchingFactor = 2;		// the maximum number of branches of the tree.

	var nodesToAddChildrenTo = new Vector();
	
	var label = null;
	var data = {'value': 0};
	var nodeId = this.treeId+'_0';
	var root = new TreeNode( nodeId, data, label);
	nodesToAddChildrenTo.addElement( root );
	var counter=1;
	
	while( nodesToAddChildrenTo.getSize() > 0 ){
		var parentNode = nodesToAddChildrenTo.getFirstElement();
		nodesToAddChildrenTo.removeFirstElement();
		var currentDepth = parentNode.getDepth();
		if( currentDepth < numberOfLevels-1 ){
			for( var i=0; i<branchingFactor; i++ ){
				// Generate a random number... if the random number is equal to 1, don't add a child node to this node. This makes the trees a bit random.
				var rand = Math.floor( Math.random()*1 ); // this will always be zero... so currently no branches are skipped
				if( i>0  && rand == 1 ){
					continue;
				}
				label = null;
				data = {'value': counter};
				nodeId = this.treeId+'_' +counter;
				var childNode = new TreeNode( nodeId, data, label);
				if( currentDepth == numberOfLevels-2 ){
					var randHeuristic = Math.floor( Math.random()*40 );
					childNode.setHeuristic( randHeuristic );
				}
				parentNode.addChild( childNode );
				nodesToAddChildrenTo.addElement( childNode );
				counter++;
			}		
		}		
	}

	
	return root;
}






//#############################################################################################################
function eightPuzzleExample( treeId, showHeuristic, algorithmA ){
	var showHeuristic = (showHeuristic == 'undefined' ) ? false : showHeuristic;
	var numberOfLevels = 4;						// the number of levels the tree has.
	var nodesToAddChildrenTo = new Vector();	// this will hold the nodes of which the children havent been added yet...
	var gridSize = 15;							// determines the gridSize of the eightPuzzles
	var states = new Vector();					// this will hold all the eightPuzzle states that have been added to the tree.
	var counter = 0;
	
	// Create the root of the tree containing the eightpuzzleUi html element.
	var eightPuzzleId = 'node_'+counter;
	var parentHtmlElement = document.createElement("div");
	parentHtmlElement.setAttribute("id",eightPuzzleId);
	parentHtmlElement.setAttribute("class","eightPuzzle");	
	// Get an eightpuzzle that is only two swaps away from the solution state.
	var eightPuzzle = new EightPuzzle();
	var numberOfChildStates = 0;
	eightPuzzle.setSolutionState();
	while( eightPuzzle.isSolution() == true || numberOfChildStates != 2){ 
		eightPuzzle = new EightPuzzle();
		eightPuzzle.setSolutionState();
		eightPuzzle.randomize( 2 );
		numberOfChildStates = eightPuzzle.getNumberOfNeighbourStates();
	}
	// Create the eightPuzzle Ui for the root node.
	var eightPuzzleUi = new EightPuzzleUi(eightPuzzleId, eightPuzzle, gridSize, parentHtmlElement );

	// Create the tree node for the root.
	var heuristic = Math.floor( Math.random()*10 ); // eightPuzzle.determineManhattanDistance(); // this determines the total manhattan distance of the eightPuzzle state.
	heuristicLabel = (algorithmA == true ) ? '0+'+heuristic+'='+(0+1+heuristic) : heuristic;
	var data = {'value' : 0, 'heuristic':heuristic};
	var nodeId = treeId+'_'+counter;
	var label = parentHtmlElement.innerHTML;
	if( showHeuristic == true ){
		label += '<span style="font-size: xx-small;">Heuristiek<br />' + heuristicLabel + '</span>';
	}
	var root = new TreeNode( nodeId, data, label );

	// Store the node info.. this contains the actual tree node and the eightpuzzle that is to be showed in that node.
	var nodeInfo = {'tree':root, 'eightPuzzle': eightPuzzle};
	nodesToAddChildrenTo.addElement( nodeInfo );
	states.addElement( eightPuzzle );

	// Keep adding children to nodes untill there are no nodes left to add children to.
	counter+=1;
	while( nodesToAddChildrenTo.getSize() > 0 ){
		// Pop a node to add children to.
		var parentNode = nodesToAddChildrenTo.getFirstElement();
		nodesToAddChildrenTo.removeFirstElement();
		var parentTree = parentNode.tree;
		var parentEightPuzzle = parentNode.eightPuzzle;

		// If the nodes are not the nodes of the bottom level... add children to them.
		var currentDepth = parentTree.getDepth();
		if( currentDepth < numberOfLevels-1 ){
			var neighbourStates = parentEightPuzzle.getNeighbourStates();
			for( var j=0; j<neighbourStates.getSize(); j++ ){
				var eightPuzzleId = 'node_'+counter;
				var eightPuzzle = neighbourStates.getElementAt(j);
				var add=true;
				/*
				for( var k=0; k<states.getSize(); k++ ){
					var state = states.getElementAt( k );
					if( state.equals( eightPuzzle ) ){
						var add=false;
					}
				}*/
				if( add==true ){
					var parentHtmlElement = document.createElement("div");
					parentHtmlElement.setAttribute("id",eightPuzzleId);
					parentHtmlElement.setAttribute("class","eightPuzzle");
					var eightPuzzleUi = new EightPuzzleUi(eightPuzzleId, eightPuzzle, gridSize, parentHtmlElement );
					
					heuristic = Math.floor( Math.random()*10 );
					heuristicLabel = (algorithmA == true ) ? (currentDepth+1)+'+'+heuristic+'='+(currentDepth+1+heuristic) : heuristic;
					label = parentHtmlElement.innerHTML;
					if( showHeuristic == true ){
						label += '<span style="font-size: xx-small;">Heuristiek<br />' + heuristicLabel + '</span>';
					}

					var value = ( eightPuzzle.isSolution() ) ? 1 : 0;

					data = {'value' : value, 'heuristic':heuristic};
					nodeId = treeId+'_'+counter;
					var childNode = new TreeNode( nodeId, data, label);

					parentTree.addChild( childNode );
					var nodeInfo = {'tree':childNode, 'eightPuzzle':eightPuzzle};
					nodesToAddChildrenTo.addElement( nodeInfo );
					counter++;
				}
			}
		}
		
	}
	//lastNode.data.value = 1;

	//document.getElementsByTagName("body")[0].appendChild( parentHtmlElement );
	return root;
}






//#############################################################################################################





function setRandomHeuristic( root, max ){
	var randomHeuristic = Math.floor( Math.random()*max+1 );
	root.setHeuristic( randomHeuristic );

	for( var i=0; i<root.getNumberOfChildren(); i++ ){
		setRandomHeuristic( root.getChild( i ),max );
	}
	return;
}